/**
 * 
 */
package com.my.pro.dao;

/**
 * @author bkoles
 * 
 */
public interface CommentDao extends Dao {
    public void removeCommentById(long commentId);
}
