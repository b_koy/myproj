package com.my.pro.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

@Entity
@Table(name = "cities")
@NamedQueries({ @NamedQuery(name = City.FIND_CITY_BY_NAME_AND_COUNTRY_NAME, query = City.FIND_CITY_BY_NAME_AND_COUNTRY_NAME_QUERY) })
public class City {
    public static final String FIND_CITY_BY_NAME_AND_COUNTRY_NAME = "City.findByNameAndCountryId";
    public static final String FIND_CITY_BY_NAME_AND_COUNTRY_NAME_QUERY = "SELECT c FROM City c WHERE c.cityName = ?1 AND c.country.countryName = ?2";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cityId", nullable = false)
    private long cityId;

    
    @Column(name = "cityName", nullable = true)
    private String cityName;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "city")
    private Set<Contact> contact;

    public City() {
    }
    
    public City(String cityName){
	this.cityName = cityName;
    }

    public long getCityId() {
	return cityId;
    }

    public void setCityId(long cityId) {
	this.cityId = cityId;
    }

    public String getCityName() {
	return cityName;
    }

    public void setCityName(String cityName) {
	this.cityName = cityName;
    }

    public Country getCountry() {
	return country;
    }

    public void setCountry(Country country) {
	this.country = country;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "countryId", nullable = false)
    private Country country;

    @Override
    public boolean equals(Object obj) {
	return this.cityName.equals(((City) obj).cityName);
    }
    
    @Override
    public int hashCode() {
        return this.cityName.hashCode();
    }
    
    @Override
    public String toString() {
      return cityName;
    }
}
