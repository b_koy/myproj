package com.my.pro.dao.implementation;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.my.pro.dao.ContactDao;
import com.my.pro.entity.Contact;

@Repository
public class ContactDaoImpl implements ContactDao{

    @PersistenceContext(name = PERSISTENCE_UNIT_NAME)
    private EntityManager entityManager;

    @Override
    public void save(Contact contact) {
	entityManager.persist(contact);
    }
}
