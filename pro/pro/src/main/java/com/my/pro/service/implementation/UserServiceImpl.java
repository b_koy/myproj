package com.my.pro.service.implementation;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.my.pro.dao.ContactDao;
import com.my.pro.dao.UserDao;
import com.my.pro.entity.Contact;
import com.my.pro.entity.Role;
import com.my.pro.entity.User;
import com.my.pro.exceptions.UserServiceException;
import com.my.pro.service.UserService;
import com.my.pro.utils.PropertyLoader;

/**
 * This service used for managing users
 * 
 * @author bkoles
 * 
 */
@Service
public class UserServiceImpl implements UserService {
    private static final Logger LOGGER = Logger
	    .getLogger(UserServiceImpl.class);
    private static final String DEFAULT_AVATAR = PropertyLoader
	    .loadProperty("userService.defaultAvatar");
    private static final String ROLE_USER = PropertyLoader
	    .loadProperty("userService.roleUser");
    private static final String ROLE_ADMIN = PropertyLoader
	    .loadProperty("userService.roleAdmin");

    @Autowired
    private UserDao userDao;

    @Autowired
    private ContactDao contactDao;

    @Transactional
    public boolean createUser(String username, String firstname,
	    String lastname, String email, String password)
	    throws UserServiceException {
	try {

	    User tempUser = new User(username, firstname, lastname, email,
		    password);
	    // tempUser.setContact(new Contact());
	    if (userDao.findByName(username) == null) {
		Role role = userDao.findRoleByName(ROLE_USER);
		tempUser.addRoleToUser(role);
		userDao.save(tempUser);
		Contact contact = new Contact();
		contact.setUser(tempUser);
		contact.setAvatar(DEFAULT_AVATAR);
		contactDao.save(contact);
		return true;

	    } else {
		return false;
	    }

	} catch (Exception e) {
	    LOGGER.error(e);
	    throw new UserServiceException("Could not create new user", e);
	}

    }

    @Transactional
    public boolean makeUserAdminByName(String username)
	    throws UserServiceException {
	User user = userDao.findByName(username);

	try {

	    if (user != null) {
		Role role = userDao.findRoleByName(ROLE_ADMIN);
		user.addRoleToUser(role);
		userDao.save(user);
		return true;

	    } else {
		return false;
	    }

	} catch (Exception e) {
	    LOGGER.error(e);
	    throw new UserServiceException("Could not make user as admin", e);
	}
    }

    @Transactional
    public User findUserByName(String username) {
	return userDao.findByName(username);
    }

}
