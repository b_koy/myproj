/**
 * 
 */
package com.my.pro.dao.implementation;

import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.my.pro.dao.AdviceDao;
import com.my.pro.entity.Advice;
import com.my.pro.entity.AdviceManager;

/**
 * @author bkoles
 * 
 */
@Repository
public class AdviceDaoImpl implements AdviceDao {

    @PersistenceContext(name = PERSISTENCE_UNIT_NAME)
    private EntityManager entityManager;

    @Override
    public Advice findAdviceById(Long id) {
	Query query = entityManager.createNamedQuery(Advice.FIND_BY_ID)
		.setParameter(1, id);
	try {
	    return (Advice) query.getSingleResult();
	} catch (NoResultException e) {
	    return null;
	}
    }

    @Override
    public void updateAdvice(Advice advice) {
	entityManager.merge(advice);
    }

    @Override
    public void removeAdvice(Advice advice) {
	Query query = entityManager.createNamedQuery(Advice.DELETE_BY_ID)
		.setParameter(1, advice.getAdviceId());
	query.executeUpdate();
    }

}
