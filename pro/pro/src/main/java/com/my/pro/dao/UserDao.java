package com.my.pro.dao;

import java.util.List;

import com.my.pro.entity.Role;
import com.my.pro.entity.User;

public interface UserDao extends Dao{

    public User findByName(String name);

    public void save(User user);

    public Role findRoleByName(String roleName);

    public void remove(User user);

    public void update(User user);

    public List<User> findAllUsers();

    public List<User> findAllUsersRange(int firstItemOmRow, int itemsPerPage);

    public Long countAllUsers();

    public List<Role> findAllRoles();

    public List<User> findUserById(String value, int firstItemOmRow,
	    int itemsPerPage);

    public Long countAllUsersById(String value);

    public List<User> findUserByUsername(String value, int firstItemOmRow,
	    int itemsPerPage);

    public Long countAllUsersByUsername(String value);

    public List<User> findUserByFirstname(String value, int firstItemOmRow,
	    int itemsPerPage);

    public Long countAllUsersByFirstname(String value);

    public List<User> findUserByLastname(String value, int firstItemOmRow,
	    int itemsPerPage);

    public Long countAllUsersByLastname(String value);

    public List<User> findUserByEmail(String value, int firstItemOmRow,
	    int itemsPerPage);

    public Long countAllUsersByEmail(String value);

}
