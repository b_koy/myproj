/**
 * 
 */
package com.my.pro.exceptions;

/**
 * @author bkoles
 * 
 */
public class AdviceServiceException extends ProException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public AdviceServiceException() {
	// TODO Auto-generated constructor stub
    }

    public AdviceServiceException(String msg) {
	super(msg);
    }

    public AdviceServiceException(String msg, Exception e) {
	super(msg, e);
    }
}
