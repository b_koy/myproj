package com.my.pro.service;

import java.util.List;

import com.my.pro.entity.Country;
import com.my.pro.entity.Role;
import com.my.pro.entity.User;
import com.my.pro.exceptions.AdminPanelServiceException;
import com.my.pro.exceptions.CityDownloaderException;

public interface AdminPanelService {

    public List<User> findAllUsers();

    public List<User> findAllUsersRange(int pageNum, int itemsOnPage);

    public List<Role> findAllRoles();

    public Role findRoleByName(String name);

    public boolean removeUser(String username)
	    throws AdminPanelServiceException;

    public boolean updateUser(User user) throws AdminPanelServiceException;

    public Long countAllUsers();

    public List<User> findUserByField(String field, String fieldValue,
	    int firstItemOmRow, int itemsPerPage);

    public Long findCountUsersByField(String field, String fieldValue);

    public boolean updateCitiesForCountry(Country country)
	    throws AdminPanelServiceException;

    public List<Country> findAllCountries();

    public boolean downloadAllCities() throws CityDownloaderException;

    public Country findCountryByName(String countryName);
}
