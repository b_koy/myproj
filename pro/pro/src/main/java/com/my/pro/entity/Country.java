package com.my.pro.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "countries")
@NamedQueries({
	@NamedQuery(name = Country.FIND_COUNTRY_BY_ID, query = Country.FIND_COUNTRY_BY_ID_QUERY),
	@NamedQuery(name = Country.FIND_ALL_COUNTRIES, query = Country.FIND_ALL_COUNTRIES_QUERY),
	@NamedQuery(name = Country.FIND_COUNTRY_BY_NAME, query = Country.FIND_COUNTRY_BY_NAME_QUERY) })
public class Country {
    public static final String FIND_COUNTRY_BY_ID = "Country.findByCountryCode";
    public static final String FIND_COUNTRY_BY_ID_QUERY = "SELECT c FROM Country c WHERE c.countryCode = ?1";
    public static final String FIND_ALL_COUNTRIES = "Country.findAllCountries";
    public static final String FIND_ALL_COUNTRIES_QUERY = "SELECT c FROM Country c";
    public static final String FIND_COUNTRY_BY_NAME = "Country.findByCountryName";
    public static final String FIND_COUNTRY_BY_NAME_QUERY = "SELECT c FROM Country c WHERE c.countryName = ?1";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "countryId", nullable = false)
    private long countryId;

    @Column(name = "countryName", nullable = true)
    private String countryName;

    @Column(name = "countryCode", nullable = false)
    private String countryCode;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "country")
    private Set<Contact> contacts;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "country", cascade = CascadeType.ALL)
    private Set<City> cities;

    public Country() {
    }

    public Country(String countryName, String countryCode) {
	this.countryName = countryName;
	this.countryCode = countryCode;
    }

    public Country(String countryName, String countryCode, Set<City> cities) {
	this.countryName = countryName;
	this.countryCode = countryCode;
	this.cities = cities;
    }

    public String getCountryCode() {
	return countryCode;
    }

    public void setCountryCode(String countryCode) {
	this.countryCode = countryCode;
    }

    public long getCountryId() {
	return countryId;
    }

    public void setCountryId(long countryId) {
	this.countryId = countryId;
    }

    public String getCountryName() {
	return countryName;
    }

    public void setCountryName(String countryName) {
	this.countryName = countryName;
    }

    public Set<Contact> getContacts() {
	return contacts;
    }

    public void setContacts(Set<Contact> contacts) {
	this.contacts = contacts;
    }

    public Set<City> getCities() {
	return cities;
    }

    public void setCities(Set<City> cities) {
	this.cities = cities;
    }

    @Override
    public boolean equals(Object obj) {
	return this.countryName.equals(((Country) obj).countryName);
    }

    @Override
    public String toString() {
	return countryName;
    }

    @Override
    public int hashCode() {
	// TODO Auto-generated method stub
	return this.countryName.hashCode();
    }
}
