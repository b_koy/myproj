package com.my.pro.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Enumeration;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.log4j.Logger;

import com.my.pro.entity.Country;
import com.my.pro.exceptions.CityDownloaderException;

public class cityDownloader {
    private static final Logger LOGGER = Logger.getLogger(cityDownloader.class);
    private static final String URL = PropertyLoader
	    .loadProperty("cityDownloader.url");
    private static final String DIR = PropertyLoader
	    .loadProperty("cityDownloader.dir");
    private static final String ZIP = PropertyLoader
	    .loadProperty("cityDownloader.zip");
    private static final String TXT = PropertyLoader
	    .loadProperty("cityDownloader.txt");

    /**
     * Download cities files by country coden
     * 
     * @param countryList
     * @return true if all downloaded
     * @throws CityDownloaderException
     */
    public static boolean downloadAllCities(List<Country> countryList)
	    throws CityDownloaderException {
	for (Country country : countryList) {
	    downloadZipFile(country.getCountryCode());
	}
	return true;
    }

    /**
     * Download file with cities by country code
     * 
     * @param fileName
     * @throws CityDownloaderException
     */
    public static void downloadZipFile(String fileName)
	    throws CityDownloaderException {
	LOGGER.info("Downloading file: " + fileName + ZIP);
	if (isFileUrlExist(URL + fileName + ZIP)) {
	    URL url;
	    try {
		url = new URL(URL + fileName + ZIP);
		HttpURLConnection connection = (HttpURLConnection) url
			.openConnection();
		connection.setRequestMethod("GET");

		InputStream in = connection.getInputStream();
		FileOutputStream out = new FileOutputStream(DIR + fileName
			+ ZIP);
		byte[] buf = new byte[1024];
		int n = in.read(buf);
		while (n >= 0) {
		    out.write(buf, 0, n);
		    n = in.read(buf);
		}
		out.flush();
		out.close();

	    } catch (IOException e) {
		throw new CityDownloaderException("Can't download files", e);
	    }
	}
    }

    /**
     * Check if file url exist
     * 
     * @param URLName
     * @return
     */
    private static boolean isFileUrlExist(String URLName) {
	try {
	    HttpURLConnection.setFollowRedirects(false);
	    // note : you may also need
	    // HttpURLConnection.setInstanceFollowRedirects(false)
	    HttpURLConnection con = (HttpURLConnection) new URL(URLName)
		    .openConnection();
	    con.setRequestMethod("HEAD");
	    return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
	} catch (Exception e) {
	    e.printStackTrace();
	    return false;
	}
    }

    /**
     * Find file with cities by country name and return city list
     * 
     * @param code
     * @return
     * @throws CityDownloaderException
     */
    public static Set<String> findCitiesByCountryCode(String code)
	    throws CityDownloaderException {
	String fileName = DIR + code + ZIP;
	String line;
	BufferedReader in;
	ZipEntry entry;
	ZipFile zipFile;
	Enumeration<? extends ZipEntry> entries;
	Set<String> cityList = null;
	if (isFileExist(fileName)) {
	    try {
		cityList = new TreeSet<>();
		zipFile = new ZipFile(fileName);
		entries = zipFile.entries();
		while (entries.hasMoreElements()) {
		    entry = entries.nextElement();
		    if (entry.getName().equals(code + TXT)) {
			InputStream stream = zipFile.getInputStream(entry);
			in = new BufferedReader(new InputStreamReader(stream,
				"UTF-8"));
			while ((line = in.readLine()) != null) {
			    String[] lineArray = line.split("\t");
			    if (!lineArray[14].equals("0"))
				cityList.add(lineArray[1]);
			}
			in.close();

		    }
		}
		zipFile.close();
	    } catch (IOException e) {
		throw new CityDownloaderException("Can't get cities", e);
	    }
	}
	return cityList;

    }

    /**
     * Checks if file exist
     * 
     * @param path
     * @return
     */
    private static boolean isFileExist(String path) {
	return new File(path).exists();

    }
}
