package com.my.pro.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "users")
@NamedQueries({
	@NamedQuery(name = User.FIND_BY_NAME, query = User.FIND_BY_NAME_QUERY),
	@NamedQuery(name = User.FIND_ALL_USERS, query = User.FIND_ALL_USERS_QUERY),
	@NamedQuery(name = User.FIND_ALL_ROLES, query = User.FIND_ALL_ROLES_QUERY),
	@NamedQuery(name = User.COUNT_ALL_USERS, query = User.COUNT_ALL_USERS_QUERY),
	@NamedQuery(name = User.FIND_USER_BY_ID, query = User.FIND_USER_BY_ID_QUERY),
	@NamedQuery(name = User.COUNT_ALL_USERS_BY_ID, query = User.COUNT_ALL_USERS_BY_ID_QUERY),
	@NamedQuery(name = User.FIND_USER_BY_FIRSTNAME, query = User.FIND_USER_BY_FIRSTNAME_QUERY),
	@NamedQuery(name = User.COUNT_ALL_USERS_BY_FIRSTNAME, query = User.COUNT_ALL_USERS_BY_FIRSTNAME_QUERY),
	@NamedQuery(name = User.FIND_USER_BY_LASTNAME, query = User.FIND_USER_BY_LASTNAME_QUERY),
	@NamedQuery(name = User.COUNT_ALL_USERS_BY_LASTNAME, query = User.COUNT_ALL_USERS_BY_LASTNAME_QUERY),
	@NamedQuery(name = User.FIND_USER_BY_EMAIL, query = User.FIND_USER_BY_EMAIL_QUERY),
	@NamedQuery(name = User.COUNT_ALL_USERS_BY_EMAIL, query = User.COUNT_ALL_USERS_BY_EMAIL_QUERY),
})
public class User {

    public static final String FIND_BY_NAME = "User.findByName";
    public static final String FIND_BY_NAME_QUERY = "SELECT u FROM User u WHERE u.username = ?1";
    public static final String COUNT_ALL_USERS_BY_NAME = "User.countAllUsersByEmail";
    public static final String COUNT_ALL_USERS_BY_NAME_QUERY = "SELECT count(u) FROM User u WHERE u.username = ?1";
    public static final String FIND_ALL_USERS = "User.findAllUsers";
    public static final String FIND_ALL_USERS_QUERY = "SELECT u FROM User u";
    public static final String FIND_ALL_ROLES = "User.findAllRoles";
    public static final String FIND_ALL_ROLES_QUERY = "SELECT r FROM Role r";
    public static final String COUNT_ALL_USERS = "User.countAllUsers";
    public static final String COUNT_ALL_USERS_QUERY = "SELECT count(u) FROM User u";
    public static final String FIND_USER_BY_ID = "User.findUserById";
    public static final String FIND_USER_BY_ID_QUERY = "SELECT u FROM User u WHERE u.userId = ?1";
    public static final String COUNT_ALL_USERS_BY_ID = "User.countAllUsersById";
    public static final String COUNT_ALL_USERS_BY_ID_QUERY = "SELECT count(u) FROM User u WHERE u.userId = ?1";
    public static final String FIND_USER_BY_FIRSTNAME = "User.findUserByFirstname";
    public static final String FIND_USER_BY_FIRSTNAME_QUERY = "SELECT u FROM User u WHERE u.firstname = ?1";
    public static final String COUNT_ALL_USERS_BY_FIRSTNAME = "User.countAllUsersByFirstname";
    public static final String COUNT_ALL_USERS_BY_FIRSTNAME_QUERY = "SELECT count(u) FROM User u WHERE u.firstname = ?1";
    public static final String FIND_USER_BY_LASTNAME = "User.findUserByLastname";
    public static final String FIND_USER_BY_LASTNAME_QUERY = "SELECT u FROM User u WHERE u.lastname = ?1";
    public static final String COUNT_ALL_USERS_BY_LASTNAME = "User.countAllUsersByLastname";
    public static final String COUNT_ALL_USERS_BY_LASTNAME_QUERY = "SELECT count(u) FROM User u WHERE u.lastname = ?1";
    public static final String FIND_USER_BY_EMAIL = "User.findUserByEmail";
    public static final String FIND_USER_BY_EMAIL_QUERY = "SELECT u FROM User u WHERE u.email = ?1";
    public static final String COUNT_ALL_USERS_BY_EMAIL = "User.countAllUsersByEmail";
    public static final String COUNT_ALL_USERS_BY_EMAIL_QUERY = "SELECT count(u) FROM User u WHERE u.email = ?1";

    @Column(name = "ctime", nullable = true)
    private Date ctime;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "firstname", nullable = true)
    private String firstname;

    @Column(name = "lastname", nullable = true)
    private String lastname;

    @Column(name = "ltime", nullable = true)
    private Date ltime;

    @Column(name = "password", nullable = false)
    private String password;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "user_roles", joinColumns = { @JoinColumn(name = "user_id", updatable = true, nullable = true) }, inverseJoinColumns = { @JoinColumn(name = "role_id", updatable = true, nullable = true) })
    @Fetch(FetchMode.JOIN)
    private Set<Role> roleSet = new HashSet<Role>();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int userId;

    @Column(name = "username", nullable = false, unique = true)
    private String username;

    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private AdviceManager adviceManager;
    
    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Contact contact;

    public Contact getContact() {
	return contact;
    }

    public void setContact(Contact contact) {
	this.contact = contact;
    }

    public User() {

    }

    public User(String username, String email, String password,
	    Set<Role> roleSet) {
	this.username = username;
	this.email = email;
	this.password = password;
	this.roleSet = roleSet;
    }

    public User(String username, String firstname, String lastname,
	    String email, String password) {
	this.username = username;
	this.firstname = firstname;
	this.lastname = lastname;
	this.email = email;
	this.password = password;
    }

    public void addRoleToUser(Role role) {
	getRoleSet().add(role);
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	User other = (User) obj;
	if (username == null) {
	    if (other.username != null)
		return false;
	} else if (!username.equals(other.username))
	    return false;
	return true;
    }

    /**
     * @return the adviceManager
     */
    public AdviceManager getAdviceManager() {
        return adviceManager;
    }

    /**
     * @param adviceManager the adviceManager to set
     */
    public void setAdviceManager(AdviceManager adviceManager) {
        this.adviceManager = adviceManager;
    }

    public Date getCtime() {
	return ctime;
    }

    public String getEmail() {
	return email;
    }

    public String getFirstname() {
	return firstname;
    }

    public String getLastname() {
	return lastname;
    }

    public Date getLtime() {
	return ltime;
    }

    public String getPassword() {
	return password;
    }

    public Set<Role> getRoleSet() {
	return roleSet;
    }

    public int getUserId() {
	return userId;
    }

    public String getUsername() {
	return username;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result
		+ ((username == null) ? 0 : username.hashCode());
	return result;
    }

    public void removeRoleFromUser(Role role) {
	getRoleSet().remove(role);
    }

    public void setCtime(Date ctime) {
	this.ctime = ctime;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public void setFirstname(String firstname) {
	this.firstname = firstname;
    }

    public void setLastname(String lastname) {
	this.lastname = lastname;
    }

    public void setLtime(Date ltime) {
	this.ltime = ltime;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    public void setRoleSet(Set<Role> roleSet) {
	this.roleSet = roleSet;
    }

    public void setUserId(int userId) {
	this.userId = userId;
    }

    public void setUsername(String username) {
	this.username = username;
    }
}
