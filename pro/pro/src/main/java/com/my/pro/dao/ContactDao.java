package com.my.pro.dao;

import com.my.pro.entity.Contact;

public interface ContactDao extends Dao{

    void save(Contact contact);
    
}
