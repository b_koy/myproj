package com.my.pro.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "contacts")
public class Contact {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contactId", nullable = false)
    private long contactId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "countryId", nullable = true)
    private Country country;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cityId", nullable = true)
    private City city;

    public City getCity() {
	return city;
    }

    public void setCity(City city) {
	this.city = city;
    }

    @Column(name = "streetName", nullable = true)
    private String streetName;

    public String getStreetName() {
	return streetName;
    }

    public void setStreetName(String streetName) {
	this.streetName = streetName;
    }

    @Column(name = "zipcode", nullable = true)
    private String zipcode;

    public String getZipcode() {
	return zipcode;
    }

    public void setZipcode(String zipcode) {
	this.zipcode = zipcode;
    }

    @Column(name = "skypeName", nullable = true)
    private String skypeName;

    @Column(name = "phoneNumber", nullable = true)
    private String phoneNumber;

    @Column(name = "site", nullable = true)
    private String site;

    @Column(name = "avatar", nullable = true)
    private String avatar;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "userId", nullable = false)
    private User user;

    public Contact() {
    }

    public long getContactId() {
	return contactId;
    }

    public void setContactId(long contactId) {
	this.contactId = contactId;
    }

    public Country getCountry() {
	return country;
    }

    public void setCountry(Country country) {
	this.country = country;
    }

    public String getSkypeName() {
	return skypeName;
    }

    public void setSkypeName(String skypeName) {
	this.skypeName = skypeName;
    }

    public String getPhoneNumber() {
	return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
	this.phoneNumber = phoneNumber;
    }

    public String getSite() {
	return site;
    }

    public void setSite(String site) {
	this.site = site;
    }

    public User getUser() {
	return user;
    }

    public void setUser(User user) {
	this.user = user;
    }

    public String getAvatar() {
	return avatar;
    }

    public void setAvatar(String avatar) {
	this.avatar = avatar;
    }
}
