package com.my.pro.dao.implementation;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.my.pro.dao.CountryDao;
import com.my.pro.entity.Country;

@Repository
public class CountryDaoImpl implements CountryDao {

    @PersistenceContext(name = PERSISTENCE_UNIT_NAME, type=PersistenceContextType.EXTENDED)
    private EntityManager entityManager;

    public CountryDaoImpl() {

    }

    public CountryDaoImpl(EntityManager entityManager) {
	this.entityManager = entityManager;
    }

    @Override
    public Country findCountryByCountryCode(String countryCode) {
	Query query = entityManager
		.createNamedQuery(Country.FIND_COUNTRY_BY_ID).setParameter(1,
			countryCode);
	try {
	    return (Country) query.getSingleResult();
	} catch (NoResultException e) {
	    return null;
	}
    }

    @Override
    public List<Country> findAllCountries() {
	Query query = entityManager
		.createNamedQuery(Country.FIND_ALL_COUNTRIES);
	return (List<Country>) query.getResultList();
    }

    @Override
    public void updateCountry(Country country) {
	entityManager.merge(country);

    }

    @Override
    public void createCountry(Country country) {
	entityManager.persist(country);
    }

    @Override
    public Country findCountryByCountryName(String countryName) {
	Query query = entityManager.createNamedQuery(
		Country.FIND_COUNTRY_BY_NAME).setParameter(1, countryName);
	try {
	    return (Country) query.getSingleResult();
	} catch (NoResultException e) {
	    return null;
	}
    }
}
