package com.my.pro.service.implementation;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.my.pro.dao.CityDao;
import com.my.pro.dao.CountryDao;
import com.my.pro.dao.UserDao;
import com.my.pro.entity.City;
import com.my.pro.entity.Contact;
import com.my.pro.entity.Country;
import com.my.pro.entity.User;
import com.my.pro.exceptions.CityDownloaderException;
import com.my.pro.exceptions.UserProfileServiceException;
import com.my.pro.service.UserProfileService;
import com.my.pro.utils.cityDownloader;

/**
 * This service used for User Profile
 * 
 * @author bkoles
 * 
 */
@Service
public class UserProfileServiceImpl implements UserProfileService {
    private final static String NONE_COUNTRY_CODE = "none";

    @Autowired
    CountryDao countryDao;

    @Autowired
    CityDao cityDao;

    @Autowired
    UserDao userDao;

    @Transactional
    public List<Country> findAllCountries() {
	return countryDao.findAllCountries();
    }

    @Transactional
    public Set<String> findCityByCountryCodeFromFile(String countryCode)
	    throws CityDownloaderException {
	return cityDownloader.findCitiesByCountryCode(countryCode);
    }

    @Transactional
    @Override
    public Country findCountryByName(String name) {
	return countryDao.findCountryByCountryName(name);
    }

    @Transactional
    @Override
    public City findCityByNameAndCountryName(String cityName, String countryName) {
	return cityDao.findCityByNameAndCountryName(cityName, countryName);
    }

    @Transactional
    @Override
    public boolean updateUserProfile(String username, String firstname,
	    String lastname, String email, String skype, String phonenumber,
	    String usercountry, String usercity, String userstreet,
	    String zipcode, String newpass) throws UserProfileServiceException {
	Country country = null;
	City city = null;
	Contact contact = null;
	try {
	    User user = userDao.findByName(username);
	    if (!firstname.equals(""))
		user.setFirstname(firstname);
	    if (!lastname.equals(""))
		user.setLastname(lastname);
	    if (!email.equals(""))
		user.setEmail(email);
	    if (!newpass.equals(""))
		user.setPassword(newpass);
	    contact = user.getContact();
	    if (!skype.equals(""))
		contact.setSkypeName(skype);
	    if (!phonenumber.equals(""))
		contact.setPhoneNumber(phonenumber);
	    if (!userstreet.equals(""))
		contact.setStreetName(userstreet);
	    if (!zipcode.equals(""))
		contact.setZipcode(zipcode);
	    if (!usercountry.equals("")) {
		country = countryDao.findCountryByCountryName(usercountry);
		city = cityDao.findCityByNameAndCountryName(usercity,
			usercountry);
		contact.setCountry(country);
		contact.setCity(city);
	    } else {
		contact.setCountry(null);
		contact.setCity(null);
	    }
	    user.setContact(contact);
	    userDao.update(user);
	    return true;
	} catch (Exception e) {
	    throw new UserProfileServiceException("Can't update user profile.",
		    e);
	}
    }

    @Transactional
    @Override
    public boolean createNewCountryOoCity(String countryName, String cityName)
	    throws UserProfileServiceException {
	try {
	    Country country = countryDao.findCountryByCountryName(countryName);
	    City city = cityDao.findCityByNameAndCountryName(cityName,
		    countryName);
	    if (country == null) {
		country = new Country(countryName, NONE_COUNTRY_CODE);
		city = new City(cityName);
		city.setCountry(country);
		Set<City> citySet = new HashSet<>();
		citySet.add(city);
		country.setCities(citySet);
		countryDao.updateCountry(country);
	    } else {
		if (city == null) {
		    city = new City(cityName);
		    city.setCountry(country);
		    Set<City> citySet = country.getCities();
		    citySet.add(city);
		    country.setCities(citySet);
		    countryDao.updateCountry(country);
		}
	    }
	    return true;
	} catch (Exception e) {
	    throw new UserProfileServiceException(
		    "Can't create new country or city", e);
	}
    }
}
