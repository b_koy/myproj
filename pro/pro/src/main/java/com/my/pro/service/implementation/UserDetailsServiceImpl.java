package com.my.pro.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.my.pro.dao.UserDao;
import com.my.pro.entity.User;

/**
 * This service used for autorisation in Spring security
 * 
 * @author bkoles
 * 
 */
@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private Assembler assembler;

    @Transactional
    public UserDetails loadUserByUsername(String username)
	    throws UsernameNotFoundException {

	UserDetails userDetails = null;

	User userEntity = userDao.findByName(username);

	if (userEntity == null) {

	    throw new UsernameNotFoundException("user not found");
	}

	userDetails = assembler.buildUserFromUserEntity(userEntity);

	return userDetails;
    }
}