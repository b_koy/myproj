package com.my.pro.controller;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.my.pro.entity.Country;
import com.my.pro.entity.Role;
import com.my.pro.entity.User;
import com.my.pro.exceptions.AdminPanelServiceException;
import com.my.pro.exceptions.CityDownloaderException;
import com.my.pro.exceptions.UserServiceException;
import com.my.pro.service.AdminPanelService;
import com.my.pro.service.UserService;
import com.my.pro.utils.PropertyLoader;
import com.my.pro.utils.UserValidator;

/**
 * Controller for Admin panel
 * 
 * @author bkoles
 * 
 */
@Controller
@RequestMapping("admin_userManager")
public class AdminUserPanelController {
    private static final Logger LOGGER = LoggerFactory
	    .getLogger(AdminUserPanelController.class);
    private static final int FIRST_PAGE = Integer.parseInt(PropertyLoader
	    .loadProperty("adminPanelController.firstPage"));
    private static final int ITEMS_ON_PAGE = Integer.parseInt(PropertyLoader
	    .loadProperty("adminPanelController.itemsOnPage"));

    @Autowired
    private AdminPanelService adminPanelService;

    @Autowired
    private UserService userService;

    /**
     * Returns User Manager Page
     * 
     * @param map
     * @return
     */
    @RequestMapping(params = "action=loadUserManager", method = RequestMethod.GET)
    public String getUserManager(Map<String, Object> map) {
	LOGGER.info("Admin Panel - User Manager");
	return "admin_userManager";
    }

    /**
     * Return Users List for page number [pageNumber] and with quantity
     * [itemsOnPage]
     * 
     * @param pageNum
     * @param itemsOnPage
     * @param map
     * @return
     */
    @RequestMapping(params = "action=findAllUsers", method = RequestMethod.POST)
    public String getAllUserList(
	    @ModelAttribute(value = "pageNum") int pageNum,
	    @ModelAttribute(value = "itemsOnPage") int itemsOnPage,
	    Map<String, Object> map) {
	LOGGER.info("Admin Panel - User Manager - All users");
	System.out.println("Size - "
		+ adminPanelService.findAllUsersRange(pageNum, itemsOnPage)
			.size());
	map.put("allUsersCount", adminPanelService.countAllUsers());
	map.put("userList",
		adminPanelService.findAllUsersRange(pageNum, itemsOnPage));
	return "admin_userManagerResult";
    }

    /**
     * Remove user with username [username] and get current numberpage and items
     * on page for updating page
     * 
     * @param username
     * @param currentPage
     * @param currentItemsOnPage
     * @param map
     * @return
     */
    @RequestMapping(params = "action=removeUser", method = RequestMethod.POST)
    public String removeUser(
	    @ModelAttribute(value = "user_name") String username,
	    @ModelAttribute(value = "currentPage") int currentPage,
	    @ModelAttribute(value = "currentItemsOnPage") int currentItemsOnPage,
	    Map<String, Object> map) {
	LOGGER.info("Admin Panel - User Manager - Remove User: " + username);
	try {
	    if (adminPanelService.removeUser(username)) {
		map.put("successMsg", "Success. User " + username + " removed.");
		map.put("allUsersCount", adminPanelService.countAllUsers());
		map.put("userList", adminPanelService.findAllUsersRange(
			currentPage, currentItemsOnPage));
		return "admin_userManagerResult";
	    }
	} catch (AdminPanelServiceException e) {
	    LOGGER.error(e.toString());
	}
	map.put("incorrectMsg", "Fail. User " + username + "didn't removed.");
	map.put("userList", adminPanelService.findAllUsersRange(currentPage,
		currentItemsOnPage));
	return "admin_userManagerResult";

    }

    /**
     * Gets user update page for user with name [username]
     * 
     * @param username
     * @param currentPage
     * @param currentItemsOnPage
     * @param map
     * @return
     */
    @RequestMapping(params = "action=updateUserGetPage", method = RequestMethod.POST)
    public String updateUserGetPage(
	    @ModelAttribute(value = "user_name") String username,
	    @ModelAttribute(value = "currentPage") int currentPage,
	    @ModelAttribute(value = "currentItemsOnPage") int currentItemsOnPage,
	    Map<String, Object> map) {
	LOGGER.info("Admin Panel - User Manager - Update User Page: "
		+ username);
	User user = userService.findUserByName(username);
	if (user != null) {
	    // System.out.println(adminPanelService.findAllRoles().toString());
	    // System.out.println(user.getRoleSet().toString());

	    map.put("user", user);
	    map.put("userRolesMap", createRoleMap(user));
	    return "admin_userUpdatePage";
	}
	map.put("incorrectMsg", "Fail. User " + username + "don't found.");
	map.put("userList", adminPanelService.findAllUsersRange(currentPage,
		currentItemsOnPage));
	map.put("allUsersCount", adminPanelService.countAllUsers());
	return "admin_userManagerResult";

    }

    /**
     * Create role map for user
     * 
     * @param user
     * @return
     */
    private Map<String, Boolean> createRoleMap(User user) {
	Map<String, Boolean> roleMap = new HashMap<>();
	Set<Role> userRoles = user.getRoleSet();
	Role tmp = null;
	for (Role role : adminPanelService.findAllRoles()) {
	    for (Role uRole : userRoles) {
		if (role.equals(uRole)) {
		    roleMap.put(role.getRole(), true);
		    tmp = role;
		    break;
		}
	    }
	    if (tmp != role)
		roleMap.put(role.getRole(), false);
	    tmp = null;
	}
	return roleMap;
    }

    /**
     * Update user
     * 
     * @param json
     * @param map
     * @return
     */
    @RequestMapping(params = "action=updateUser", method = RequestMethod.POST)
    public String updateUser(@ModelAttribute(value = "json") String json,
	    Map<String, Object> map) {
	// System.out.println(json);
	String username = "current";
	int currentPage = FIRST_PAGE;
	int currentItemsOnPage = ITEMS_ON_PAGE;
	try {
	    JSONObject jsonn = new JSONObject(json);
	    username = (String) jsonn.get("username");
	    String firstname = (String) jsonn.get("firstname");
	    String lastname = (String) jsonn.get("lastname");
	    String email = (String) jsonn.get("email");
	    String password = (String) jsonn.get("password");
	    JSONArray jsonArr = jsonn.getJSONArray("roles");
	    currentPage = (int) jsonn.get("currentPage");
	    currentItemsOnPage = Integer.parseInt((String) jsonn
		    .get("currentItemsOnPage"));
	    if (UserValidator.isCorrectName(username)
		    && UserValidator.isCorrectFirstName(firstname)
		    && UserValidator.isCorrectLastName(lastname)
		    && UserValidator.isCorrectEmail(email)
		    && UserValidator.isCorrectPassword(password)) {
		Set<Role> roleSet = new HashSet<>();
		for (int i = 0; i < jsonArr.length(); i++) {
		    Role role = adminPanelService
			    .findRoleByName((String) jsonArr.get(i));
		    roleSet.add(role);
		}
		LOGGER.info("Admin Panel - User Manager - Update User: "
			+ username);
		User user = userService.findUserByName(username);
		user.setFirstname(firstname);
		user.setLastname(lastname);
		user.setEmail(email);
		user.setPassword(password);
		user.setRoleSet(roleSet);

		if (adminPanelService.updateUser(user)) {
		    LOGGER.info("Admin Panel - User Manager - Update User: "
			    + username + " Updated!");
		    map.put("successMsg", "Success. User " + username
			    + " updated.");
		    map.put("userList", adminPanelService.findAllUsersRange(
			    currentPage, currentItemsOnPage));
		    map.put("allUsersCount", adminPanelService.countAllUsers());
		    return "admin_userManagerResult";
		}
	    }
	    map.put("incorrectMsg", "Incorrect data!");
	    map.put("userList", adminPanelService.findAllUsersRange(
		    currentPage, currentItemsOnPage));
	    map.put("allUsersCount", adminPanelService.countAllUsers());
	    return "admin_userManagerResult";

	} catch (JSONException | AdminPanelServiceException e) {
	    LOGGER.error(e.toString());
	    e.printStackTrace();
	    map.put("errorMsg", "Fail. User " + username + "didn't updated.");
	    map.put("userList", adminPanelService.findAllUsersRange(
		    currentPage, currentItemsOnPage));
	    map.put("allUsersCount", adminPanelService.countAllUsers());
	    return "admin_userManagerResult";
	}
    }

    /**
     * Shows user roles
     * 
     * @param username
     * @param map
     * @return
     */
    @Deprecated
    @RequestMapping(params = "action=showUserRoles", method = RequestMethod.POST)
    public String showUserRoles(
	    @ModelAttribute(value = "user_name") String username,
	    Map<String, Object> map) {
	LOGGER.info("Admin Panel - User Manager - Show User Roles: " + username);
	User user = userService.findUserByName(username);

	if (user != null) {

	    // map.put("userList",
	    // adminPanelService.findAllUsersRange(FIRST_PAGE,
	    // ITEMS_ON_PAGE));
	    map.put("currentUserRoles", user.getRoleSet());
	    return "admin_userUpdatePage";
	}
	map.put("incorrectMsg", "Fail. User " + username + "don't found.");
	map.put("userList",
		adminPanelService.findAllUsersRange(FIRST_PAGE, ITEMS_ON_PAGE));
	map.put("allUsersCount", adminPanelService.countAllUsers());
	return "admin_userManagerResult";
    }

    /**
     * Return add new user page
     * 
     * @return
     */
    @RequestMapping(params = "action=addNewUserPage", method = RequestMethod.GET)
    public String addUserPage() {
	return "admin_addNewUserPage";
    }

    /**
     * Add new user
     * 
     * @param username
     * @param firstname
     * @param lastname
     * @param email
     * @param pass
     * @param currentPage
     * @param currentItemsOnPage
     * @param map
     * @return
     */
    @RequestMapping(params = "action=addNewUser", method = RequestMethod.POST)
    public String addUser(
	    @ModelAttribute(value = "user_name") String username,
	    @ModelAttribute(value = "first_name") String firstname,
	    @ModelAttribute(value = "last_name") String lastname,
	    @ModelAttribute(value = "e-mail") String email,
	    @ModelAttribute(value = "pass") String pass,
	    @ModelAttribute(value = "currentPage") int currentPage,
	    @ModelAttribute(value = "currentItemsOnPage") int currentItemsOnPage,
	    Map<String, Object> map) {
	LOGGER.info("Admin Panel - User Manager - Add new user: " + username);
	try {

	    if (UserValidator.isCorrectName(username)
		    && UserValidator.isCorrectFirstName(firstname)
		    && UserValidator.isCorrectLastName(lastname)
		    && UserValidator.isCorrectEmail(email)
		    && UserValidator.isCorrectPassword(pass)) {

		if (userService.createUser(username, firstname, lastname,
			email, pass)) {
		    map.put("successMsg", "User was registred");
		    map.put("userList", adminPanelService.findAllUsersRange(
			    currentPage, currentItemsOnPage));
		    map.put("allUsersCount", adminPanelService.countAllUsers());
		    return "admin_userManagerResult";
		}
		map.put("incorrectMsg", "Current User exist");
		map.put("userList", adminPanelService.findAllUsersRange(
			currentPage, currentItemsOnPage));
		map.put("allUsersCount", adminPanelService.countAllUsers());
		return "admin_userManagerResult";

	    } else {
		map.put("incorrectMsg", "Incorrect data!");
		map.put("userList", adminPanelService.findAllUsersRange(
			currentPage, currentItemsOnPage));
		map.put("allUsersCount", adminPanelService.countAllUsers());
		return "admin_userManagerResult";
	    }

	} catch (UserServiceException e) {
	    // map.put("errorList", ExceptionUtil.createErrorList(e));
	    map.put("errorMsg", e.getMessage());
	    map.put("userList", adminPanelService.findAllUsersRange(
		    currentPage, currentItemsOnPage));
	    map.put("allUsersCount", adminPanelService.countAllUsers());
	    return "admin_userManagerResult";
	}
    }

    /**
     * Find you user by field(name, lastname ...)
     * 
     * @param field
     * @param fieldValue
     * @param pageNum
     * @param itemsOnPage
     * @param map
     * @return
     */
    @RequestMapping(params = "action=findUserByField", method = RequestMethod.POST)
    public String findUserByFild(@ModelAttribute(value = "field") String field,
	    @ModelAttribute(value = "fieldValue") String fieldValue,
	    @ModelAttribute(value = "pageNum") int pageNum,
	    @ModelAttribute(value = "itemsOnPage") int itemsOnPage,
	    Map<String, Object> map) {
	List<User> userList = adminPanelService.findUserByField(field,
		fieldValue, pageNum, itemsOnPage);

	if (userList != null) {
	    map.put("userList", userList);
	    map.put("allUsersCount",
		    adminPanelService.findCountUsersByField(field, fieldValue));
	    return "admin_userManagerResult";
	}
	map.put("incorrectMsg", "Didn't find enything");
	return "admin_userManagerResult";
    }

    /**
     * Start download and update cities in data base from geodata.org
     * 
     * @return
     */
    @RequestMapping(params = "action=downloadAndUpdateCities", method = RequestMethod.GET)
    @ResponseBody
    public String downloadAndUpdateCities() {
	LOGGER.info("Admin Panel - City download and update ");
	try {
	    adminPanelService.downloadAllCities();
	    for (Country country : adminPanelService.findAllCountries()) {
		adminPanelService.updateCitiesForCountry(country);
	    }
	    return "Cities downloaded and updated successful.";
	} catch (AdminPanelServiceException | CityDownloaderException e) {
	    e.printStackTrace();
	    return "Error. Cities didn't downloaded and updated.";
	}
    }

}
