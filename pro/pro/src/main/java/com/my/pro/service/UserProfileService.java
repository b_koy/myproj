package com.my.pro.service;

import java.util.List;
import java.util.Set;

import com.my.pro.entity.City;
import com.my.pro.entity.Country;
import com.my.pro.exceptions.CityDownloaderException;
import com.my.pro.exceptions.UserProfileServiceException;

public interface UserProfileService {

    public Set<String> findCityByCountryCodeFromFile(String countryCode)
	    throws CityDownloaderException;

    public List<Country> findAllCountries();

    Country findCountryByName(String name);

    public City findCityByNameAndCountryName(String cityName, String countryName);

    boolean createNewCountryOoCity(String countryName, String cityName)
	    throws UserProfileServiceException;

	boolean updateUserProfile(String username, String firstname,
			String lastname, String email, String skype, String phonenumber,
			String usercountry, String usercity, String userstreet,
			String zipcode, String newpass) throws UserProfileServiceException;
        
}
