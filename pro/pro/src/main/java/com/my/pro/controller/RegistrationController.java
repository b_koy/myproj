package com.my.pro.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.my.pro.entity.User;
import com.my.pro.exceptions.UserServiceException;
import com.my.pro.service.UserService;
import com.my.pro.utils.UserValidator;

/**
 * Controller for registration
 * 
 * @author bkoles
 * 
 */
@Controller
public class RegistrationController {
    private static final Logger LOGGER = LoggerFactory
	    .getLogger(RegistrationController.class);
    @Autowired
    private UserService userService;

    /**
     * Return registration page
     * 
     * @return
     */
    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registration() {
	return "registration";
    }

    /**
     * Register new user
     * 
     * @param user_name
     * @param first_name
     * @param last_name
     * @param email
     * @param pass
     * @param map
     * @return
     */
    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String addUser(
	    @ModelAttribute(value = "user_name") String user_name,
	    @ModelAttribute(value = "first_name") String first_name,
	    @ModelAttribute(value = "last_name") String last_name,
	    @ModelAttribute(value = "e-mail") String email,
	    @ModelAttribute(value = "pass") String pass, Map<String, Object> map) {

	try {

	    if (UserValidator.isCorrectName(user_name)
		    && UserValidator.isCorrectFirstName(first_name)
		    && UserValidator.isCorrectLastName(last_name)
		    && UserValidator.isCorrectEmail(email)
		    && UserValidator.isCorrectPassword(pass)) {
		return addUserDB(userService.createUser(user_name, first_name,
			last_name, email, pass), map);

	    } else {
		map.put("incorrectMsg", "Incorrect data!");
		return "result";
	    }

	} catch (UserServiceException e) {
	    // map.put("errorList", ExceptionUtil.createErrorList(e));
	    map.put("errorMsg", e.getMessage());
	    return "result";
	}
    }

    private String addUserDB(boolean flag, Map<String, Object> map) {

	if (flag) {
	    map.put("successMsg", "User was registred");
	    return "result";
	}
	map.put("incorrectMsg", "Current User exist");
	return "result";
    }

    /**
     * Check if user already exist
     * 
     * @param username
     * @return
     */
    @RequestMapping(value = "/checkUserName", method = RequestMethod.POST)
    @ResponseBody
    public String checkUserName(
	    @ModelAttribute(value = "userName") String username) {
	LOGGER.info("Check user name " + username);
	User user = userService.findUserByName(username);
	if (user == null)
	    return "true";
	return "false";
    }
}
