package com.my.pro.exceptions;

public class AdminPanelServiceException extends ProException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    public AdminPanelServiceException() {
	// TODO Auto-generated constructor stub
    }
    
    public AdminPanelServiceException(String msg) {
	super(msg);
    }

    public AdminPanelServiceException(String msg, Exception e) {
	super(msg, e);
    }
    

}
