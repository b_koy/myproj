package com.my.pro.exceptions;

public class CityDownloaderException extends ProException {
    /**
     * 
     */
    private static final long serialVersionUID = 1260227947441206969L;

    public CityDownloaderException() {
    }

    public CityDownloaderException(String msg) {
	super(msg);
    }

    public CityDownloaderException(String msg, Exception e) {
	super(msg, e);
    }

}
