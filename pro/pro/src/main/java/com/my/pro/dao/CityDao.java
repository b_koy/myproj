package com.my.pro.dao;

import com.my.pro.entity.City;

public interface CityDao extends Dao {
    public City findCityByNameAndCountryName(String cityName, String countryName);
}
