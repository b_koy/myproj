package com.my.pro.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.my.pro.entity.City;
import com.my.pro.entity.Contact;
import com.my.pro.entity.Country;
import com.my.pro.entity.User;
import com.my.pro.exceptions.AdminPanelServiceException;
import com.my.pro.exceptions.UserProfileServiceException;
import com.my.pro.service.AdminPanelService;
import com.my.pro.service.UserProfileService;
import com.my.pro.service.UserService;
import com.my.pro.utils.PropertyLoader;

/**
 * Controller for Edit profile
 * 
 * @author bkoles
 * 
 */
@Controller
@RequestMapping("secure_userProfile")
public class UserProfileController {
    private static final Logger LOGGER = LoggerFactory
	    .getLogger(UserProfileController.class);
    private static final String USER_IMAGES_PATH = PropertyLoader
	    .loadProperty("userProfileController.userImagePath");
    private static final String USER_IMAGES_UPLOAD_PATH = PropertyLoader
	    .loadProperty("userProfileController.userImageUploadPath");
    private static final String USER_IMAGE_NAME = PropertyLoader
	    .loadProperty("userProfileController.userImgName");

    @Autowired
    private UserService userService;

    @Autowired
    private UserProfileService userProfileService;

    @Autowired
    private AdminPanelService adminPanelService;

    /**
     * Return profile page
     * 
     * @param username
     * @param map
     * @return
     */
    @PreAuthorize("hasRole('ROLE_USER')")
    @RequestMapping(params = "action=getProfilePage", method = RequestMethod.GET)
    public String getProfilePage(
	    @ModelAttribute(value = "username") String username,
	    Map<String, Object> map) {
	LOGGER.info("User Profile page - " + username);
	map.put("user", userService.findUserByName(username));
	return "user_userProfilePage";
    }

    /**
     * Return edit profile page
     * 
     * @param username
     * @param map
     * @return
     */
    @RequestMapping(params = "action=getProfileEditPage", method = RequestMethod.GET)
    public String getProfileEditPage(
	    @ModelAttribute(value = "username") String username,
	    Map<String, Object> map) {
	LOGGER.info("User Profile Edit page - " + username);
	User user = userService.findUserByName(username);
	// List<Country> countryList = userProfileService.findAllCountries();
	map.put("user", user);
	return "user_userProfileEditPage";
    }

    /**
     * Return string with all countries for autocomplete
     * 
     * @param map
     * @return
     */
    @RequestMapping(params = "action=getCountryList", method = RequestMethod.GET)
    @ResponseBody
    public String findAllCountries(Map<String, Object> map) {
	List<String> countryList = new ArrayList<>();

	for (Country country : userProfileService.findAllCountries()) {
	    countryList.add(country.toString());
	}

	return countryList.toString().replace(", ", ":").replace("[", "")
		.replace("]", "");
    }

    /**
     * Return string with all cities by country name for autocomplete
     * 
     * @param countryName
     * @param map
     * @return
     */
    @RequestMapping(params = "action=getCityList", method = RequestMethod.POST)
    @ResponseBody
    public String findAllCitiesByCountry(
	    @ModelAttribute(value = "countryName") String countryName,
	    Map<String, Object> map) {
	List<String> cityList = new ArrayList<>();
	Country country = userProfileService.findCountryByName(countryName);

	if ((country != null) && (country.getCities() != null)) {
	    for (City city : country.getCities()) {
		cityList.add(city.toString());
	    }
	}
	return (cityList.size() != 0 ? cityList.toString().replace(", ", ":")
		.replace("[", "").replace("]", "") : "");
    }

    /**
     * Update user profile
     * 
     * @param username
     * @param firstname
     * @param lastname
     * @param email
     * @param skype
     * @param phonenumber
     * @param usercountry
     * @param usercity
     * @param userstreet
     * @param zipcode
     * @param newpass
     * @param map
     * @return
     */
    @RequestMapping(params = "action=updateUserProfile", method = RequestMethod.POST)
    public String updateUserProfile(
	    @ModelAttribute(value = "username") String username,
	    @ModelAttribute(value = "firstname") String firstname,
	    @ModelAttribute(value = "lastname") String lastname,
	    @ModelAttribute(value = "email") String email,
	    @ModelAttribute(value = "skype") String skype,
	    @ModelAttribute(value = "phonenumber") String phonenumber,
	    @ModelAttribute(value = "usercountry") String usercountry,
	    @ModelAttribute(value = "usercity") String usercity,
	    @ModelAttribute(value = "userstreet") String userstreet,
	    @ModelAttribute(value = "zipcode") String zipcode,
	    @ModelAttribute(value = "newpass") String newpass,
	    Map<String, Object> map) {
	LOGGER.info("User Profile Edit page - Updating User Profile "
		+ username);
	try {
	    if ((userProfileService.findCountryByName(usercountry) == null)
		    || (userProfileService.findCityByNameAndCountryName(
			    usercity, usercountry) == null)) {
		userProfileService
			.createNewCountryOoCity(usercountry, usercity);
	    }
	    userProfileService.updateUserProfile(username, firstname, lastname,
		    email, skype, phonenumber, usercountry, usercity,
		    userstreet, zipcode, newpass);
	    map.put("user", userService.findUserByName(username));
	    return "user_userProfilePage";
	} catch (UserProfileServiceException e) {
	    e.printStackTrace();
	    return "ErrorPage";
	}

    }

    /**
     * Upload avatar for user
     * 
     * @param countryName
     * @param map
     * @return
     */
    @RequestMapping(params = "action=uploadAvatar", method = RequestMethod.POST)
    @ResponseBody
    public String uploadAvatarForUser(
	    @RequestParam(value = "username", required = false) String username,
	    @RequestParam(value = "file", required = false) MultipartFile image,
	    Map<String, Object> map) {
	LOGGER.info("User Profile Edit page - Uploading avatar");

	File file = null;
	// Creating path for uploading and page
	String imgPath = USER_IMAGES_PATH + username + USER_IMAGE_NAME;
	String imgUploadPath = USER_IMAGES_UPLOAD_PATH + username
		+ USER_IMAGE_NAME;
	try {
	    // Creating folder for user
	    new File(USER_IMAGES_UPLOAD_PATH + username).mkdir();
	    // Creating new avatar file
	    file = new File(imgUploadPath);
	    FileUtils.writeByteArrayToFile(file, image.getBytes());
	} catch (IOException e) {
	    e.printStackTrace();
	    return "Can't upload new image";
	}
	// Updating user avatar
	User user = userService.findUserByName(username);
	Contact contact = user.getContact();
	contact.setAvatar(imgPath);
	user.setContact(contact);
	try {
	    adminPanelService.updateUser(user);
	} catch (AdminPanelServiceException e) {
	    e.printStackTrace();
	    return "Can't update user image";
	}

	return " File uploaded";
    }
}
