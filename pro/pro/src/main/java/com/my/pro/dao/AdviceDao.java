/**
 * 
 */
package com.my.pro.dao;

import com.my.pro.entity.Advice;

/**
 * @author bkoles
 * 
 */
public interface AdviceDao extends Dao {
    public Advice findAdviceById(Long id);

    public void updateAdvice(Advice advice);
    
    public void removeAdvice(Advice advice);
}
