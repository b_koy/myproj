package com.my.pro.utils;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class CityBuilderJson {

    public static  Map<String, List<String>> getCityMapFromJson(String file)
	    throws FileNotFoundException, IOException, ParseException {
	JSONParser parser = new JSONParser();
	JSONObject json = (JSONObject) parser.parse(new  FileReader(file));
	Set<String> countryCode = json.keySet();
	Map<String, List<String>> map = new HashMap<>();
	for (String country : countryCode) {
	    List<String> cityList = (List<String>) json.get(country);
	    map.put(country, cityList);
	}
	return map;
    }
}
