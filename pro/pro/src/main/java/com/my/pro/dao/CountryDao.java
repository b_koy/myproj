package com.my.pro.dao;

import java.util.List;

import com.my.pro.entity.Country;

public interface CountryDao extends Dao {
    public Country findCountryByCountryCode(String countryCode);

    public List<Country> findAllCountries();

    public void updateCountry(Country country);

    public void createCountry(Country country);

    public Country findCountryByCountryName(String countryName);
}
