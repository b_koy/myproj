package com.my.pro.exceptions;

public class UserProfileServiceException extends ProException {
    /**
     * 
     */
    private static final long serialVersionUID = -3268190862838130215L;

    public UserProfileServiceException() {
    }

    public UserProfileServiceException(String msg) {
	super(msg);
    }

    public UserProfileServiceException(String msg, Exception e) {
	super(msg, e);
    }

}
