package com.my.pro.service;

import com.my.pro.entity.Advice;
import com.my.pro.exceptions.AdviceServiceException;

/**
 * @author bkoles
 * 
 */
public interface AdviceService {
    public boolean addAdvice(String adviceName, String adviceText,
	    String username) throws AdviceServiceException;

    public Advice findAdviceById(long id) throws AdviceServiceException;

    public boolean updateAdvice(long adviceId, String adviceName,
	    String adviceText, String username) throws AdviceServiceException;

    public boolean removeAdvice(long adviceId, String username)
	    throws AdviceServiceException;

    public boolean addCommentToAdvice(long adviceId, String commentText,
	    String username) throws AdviceServiceException;

    public boolean removeCommentById(long commentId)
	    throws AdviceServiceException;
}
