package com.my.pro.dao.implementation;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.my.pro.dao.CityDao;
import com.my.pro.entity.City;

@Repository
public class CityDaoImpl implements CityDao {

    @PersistenceContext(name = PERSISTENCE_UNIT_NAME, type = PersistenceContextType.EXTENDED)
    private EntityManager entityManager;

    @Override
    public City findCityByNameAndCountryName(String cityName, String countryName) {
	Query query = entityManager
		.createNamedQuery(City.FIND_CITY_BY_NAME_AND_COUNTRY_NAME)
		.setParameter(1, cityName).setParameter(2, countryName);
	try {
	    List<City> cityList = (List<City>) query.getResultList();
	    return (cityList.size() != 0 ? cityList.get(0) : null);
	} catch (NoResultException e) {
	    return null;
	}
    }
}
