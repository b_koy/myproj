package com.my.pro.entity;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "advices")
@NamedQueries({
	@NamedQuery(name = Advice.FIND_BY_ID, query = Advice.FIND_BY_ID_QUERY),
	@NamedQuery(name = Advice.DELETE_BY_ID, query = Advice.DELETE_BY_ID_QUERY), })
public class Advice {
    public static final String FIND_BY_ID = "Advice.findById";
    public static final String FIND_BY_ID_QUERY = "SELECT a FROM Advice a WHERE a.adviceId = ?1";
    public static final String DELETE_BY_ID = "Advice.deleteById";
    public static final String DELETE_BY_ID_QUERY = "DELETE FROM Advice a WHERE a.adviceId = ?1";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "adviceId", nullable = false)
    private long adviceId;

    @Column(name = "adviceName", nullable = false)
    private String adviceName;

    @Column(name = "ctime", nullable = true)
    private Date ctime;

    @Column(name = "last_edit_time", nullable = true)
    private Date letime;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "aManagerId", nullable = false)
    private AdviceManager adviceManager;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "advice", cascade = CascadeType.ALL)
    private Set<Comment> comments;

    @Column(name = "adviceFilePath", nullable = true)
    private String adviceFilePath;

    @Column(name = "adviceNumberInManager", nullable = false)
    private long adviceNumberInManager;

    /**
     * @return the letime
     */
    public Date getLetime() {
	return letime;
    }

    /**
     * @param letime
     *            the letime to set
     */
    public void setLetime(Date letime) {
	this.letime = letime;
    }

    /**
     * @return the adviceFilePath
     */
    public String getAdviceFilePath() {
	return adviceFilePath;
    }

    /**
     * @return the adviceNumberInManager
     */
    public long getAdviceNumberInManager() {
	return adviceNumberInManager;
    }

    /**
     * @param adviceNumberInManager
     *            the adviceNumberInManager to set
     */
    public void setAdviceNumberInManager(long adviceNumberInManager) {
	this.adviceNumberInManager = adviceNumberInManager;
    }

    /**
     * @param adviceFilePath
     *            the adviceFilePath to set
     */
    public void setAdviceFilePath(String adviceFilePath) {
	this.adviceFilePath = adviceFilePath;
    }

    /**
     * 
     */
    public Advice() {
	super();
    }

    /**
     * @return the adviceId
     */
    public long getAdviceId() {
	return adviceId;
    }

    /**
     * @param adviceId
     *            the adviceId to set
     */
    public void setAdviceId(long adviceId) {
	this.adviceId = adviceId;
    }

    /**
     * @return the advicename
     */
    public String getAdviceName() {
	return adviceName;
    }

    /**
     * @param advicename
     *            the advicename to set
     */
    public void setAdviceName(String advicename) {
	this.adviceName = advicename;
    }

    /**
     * @return the ctime
     */
    public Date getCtime() {
	return ctime;
    }

    /**
     * @param ctime
     *            the ctime to set
     */
    public void setCtime(Date ctime) {
	this.ctime = ctime;
    }

    /**
     * @return the adviceManager
     */
    public AdviceManager getAdviceManager() {
	return adviceManager;
    }

    /**
     * @param adviceManager
     *            the adviceManager to set
     */
    public void setAdviceManager(AdviceManager adviceManager) {
	this.adviceManager = adviceManager;
    }

    /**
     * @return the comments
     */
    public Set<Comment> getComments() {
	return comments;
    }

    /**
     * @param comments
     *            the comments to set
     */
    public void setComments(Set<Comment> comments) {
	this.comments = comments;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return "Advice [adviceId=" + adviceId + ", adviceName=" + adviceName
		+ ", ctime=" + ctime + ", letime=" + letime + ", comments="
		+ comments + ", adviceFilePath=" + adviceFilePath
		+ ", adviceNumberInManager=" + adviceNumberInManager + "]";
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result
		+ ((adviceFilePath == null) ? 0 : adviceFilePath.hashCode());
	result = prime * result + (int) (adviceId ^ (adviceId >>> 32));
	result = prime * result
		+ ((adviceName == null) ? 0 : adviceName.hashCode());
	result = prime
		* result
		+ (int) (adviceNumberInManager ^ (adviceNumberInManager >>> 32));
	// result = prime * result
	// + ((comments == null) ? 0 : comments.hashCode());
	result = prime * result + ((ctime == null) ? 0 : ctime.hashCode());
	result = prime * result + ((letime == null) ? 0 : letime.hashCode());
	return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Advice other = (Advice) obj;
	if (adviceFilePath == null) {
	    if (other.adviceFilePath != null)
		return false;
	} else if (!adviceFilePath.equals(other.adviceFilePath))
	    return false;
	if (adviceId != other.adviceId)
	    return false;
	if (adviceName == null) {
	    if (other.adviceName != null)
		return false;
	} else if (!adviceName.equals(other.adviceName))
	    return false;
	if (adviceNumberInManager != other.adviceNumberInManager)
	    return false;
	if (comments == null) {
	    if (other.comments != null)
		return false;
	} else if (!comments.equals(other.comments))
	    return false;
	if (ctime == null) {
	    if (other.ctime != null)
		return false;
	} else if (!ctime.equals(other.ctime))
	    return false;
	if (letime == null) {
	    if (other.letime != null)
		return false;
	} else if (!letime.equals(other.letime))
	    return false;
	return true;
    }

}
