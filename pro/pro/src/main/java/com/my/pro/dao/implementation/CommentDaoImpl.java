/**
 * 
 */
package com.my.pro.dao.implementation;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.my.pro.dao.CommentDao;
import com.my.pro.entity.Comment;

/**
 * @author bkoles
 * 
 */
@Repository
public class CommentDaoImpl implements CommentDao {
    @PersistenceContext(name = PERSISTENCE_UNIT_NAME)
    private EntityManager entityManager;

    @Override
    public void removeCommentById(long commentId) {
	Query query = entityManager.createNamedQuery(Comment.DELETE_BY_ID)
		.setParameter(1, commentId);
	query.executeUpdate();
    }
}
