package com.my.pro.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "comments")
@NamedQueries({
	@NamedQuery(name = Comment.DELETE_BY_ID, query = Comment.DELETE_BY_ID_QUERY), })
public class Comment {
    public static final String DELETE_BY_ID = "Comment.deleteById";
    public static final String DELETE_BY_ID_QUERY = "DELETE FROM Comment c WHERE c.commentId = ?1";
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "commentId", nullable = false)
    private long commentId;

    @Column(name = "text", nullable = false)
    private String commentText;

    @Column(name = "ctime", nullable = true)
    private Date ctime;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "adviceId", nullable = false)
    private Advice advice;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "aManagerId", nullable = false)
    private AdviceManager adviceManager;
    
    /**
     * @return the adviceManager
     */
    public AdviceManager getAdviceManager() {
        return adviceManager;
    }

    /**
     * @param adviceManager the adviceManager to set
     */
    public void setAdviceManager(AdviceManager adviceManager) {
        this.adviceManager = adviceManager;
    }

    /**
     * @return the commentId
     */
    public long getCommentId() {
        return commentId;
    }

    /**
     * @param commentId the commentId to set
     */
    public void setCommentId(long commentId) {
        this.commentId = commentId;
    }

    /**
     * @return the commentText
     */
    public String getCommentText() {
        return commentText;
    }

    /**
     * @param commentText the commentText to set
     */
    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    /**
     * @return the ctime
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * 
     */
    public Comment() {
	super();
    }
    
    

   
    /**
     * @param commentText
     * @param ctime
     * @param advice
     */
    public Comment(String commentText, Date ctime, Advice advice, AdviceManager adviceManager) {
	super();
	this.commentText = commentText;
	this.ctime = ctime;
	this.advice = advice;
	this.adviceManager = adviceManager;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result
		+ ((adviceManager == null) ? 0 : adviceManager.hashCode());
	result = prime * result + (int) (commentId ^ (commentId >>> 32));
	result = prime * result
		+ ((commentText == null) ? 0 : commentText.hashCode());
	result = prime * result + ((ctime == null) ? 0 : ctime.hashCode());
	return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Comment other = (Comment) obj;
	if (adviceManager == null) {
	    if (other.adviceManager != null)
		return false;
	} else if (!adviceManager.equals(other.adviceManager))
	    return false;
	if (commentId != other.commentId)
	    return false;
	if (commentText == null) {
	    if (other.commentText != null)
		return false;
	} else if (!commentText.equals(other.commentText))
	    return false;
	if (ctime == null) {
	    if (other.ctime != null)
		return false;
	} else if (!ctime.equals(other.ctime))
	    return false;
	return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return "Comment [commentId=" + commentId + ", commentText="
		+ commentText + ", ctime=" + ctime + ", adviceManager="
		+ adviceManager + "]";
    }

    /**
     * @param ctime the ctime to set
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    /**
     * @return the advice
     */
    public Advice getAdvice() {
        return advice;
    }

    /**
     * @param advice the advice to set
     */
    public void setAdvice(Advice advice) {
        this.advice = advice;
    }

    
}
