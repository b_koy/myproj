package com.my.pro.exceptions;

public class UserServiceException extends ProException {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public UserServiceException() {
    }

    public UserServiceException(String msg) {
	super(msg);
    }

    public UserServiceException(String msg, Exception e) {
	super(msg, e);
    }
}
