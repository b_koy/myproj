package com.my.pro.dao.implementation;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.my.pro.dao.UserDao;
import com.my.pro.entity.Role;
import com.my.pro.entity.User;

@Repository
public class UserDaoImpl implements UserDao {

    @PersistenceContext(name = PERSISTENCE_UNIT_NAME)
    private EntityManager entityManager;

    public UserDaoImpl() {

    }

    public UserDaoImpl(EntityManager entityManager) {
	this.entityManager = entityManager;
    }

    public User findByName(String name) {
	Query query = entityManager.createNamedQuery(User.FIND_BY_NAME)
		.setParameter(1, name);
	try {
	    return (User) query.getSingleResult();
	} catch (NoResultException e) {
	    return null;
	}
    }

    public Role findRoleByName(String roleName) {
	Query query = entityManager.createNamedQuery(Role.FIND_BY_NAME)
		.setParameter(1, roleName);
	try {
	    return (Role) query.getSingleResult();
	} catch (NoResultException e) {
	    return null;
	}
    }

    public void remove(User user) {
	user.getRoleSet().clear();
	entityManager.remove(user);
    }

    public void save(User user) {
	entityManager.persist(user);
    }

    public void update(User user) {
	entityManager.merge(user);

    }

    public List<User> findAllUsers() {
	Query query = entityManager.createNamedQuery(User.FIND_ALL_USERS);
	return (List<User>) query.getResultList();
    }

    public List<User> findAllUsersRange(int firstItemOmRow, int itemsPerPage) {
	Query query = entityManager.createNamedQuery(User.FIND_ALL_USERS);
	query = query.setFirstResult(firstItemOmRow)
		.setMaxResults(itemsPerPage);
	return (List<User>) query.getResultList();
    }

    public List<Role> findAllRoles() {
	Query query = entityManager.createNamedQuery(User.FIND_ALL_ROLES);
	return (List<Role>) query.getResultList();
    }

    public Long countAllUsers() {
	Query query = entityManager.createNamedQuery(User.COUNT_ALL_USERS);
	return (Long) query.getSingleResult();
    }

    public List<User> findUserById(String value, int firstItemOmRow,
	    int itemsPerPage) {
	Query query = entityManager.createNamedQuery(User.FIND_USER_BY_ID)
		.setParameter(1, value);
	query = query.setFirstResult(firstItemOmRow)
		.setMaxResults(itemsPerPage);
	return (List<User>) query.getResultList();
    }

    public Long countAllUsersById(String value) {
	Query query = entityManager
		.createNamedQuery(User.COUNT_ALL_USERS_BY_ID).setParameter(1,
			value);
	return (Long) query.getSingleResult();
    }

    public List<User> findUserByUsername(String value, int firstItemOmRow,
	    int itemsPerPage) {
	Query query = entityManager.createNamedQuery(User.FIND_BY_NAME)
		.setParameter(1, value);
	query = query.setFirstResult(firstItemOmRow)
		.setMaxResults(itemsPerPage);
	return (List<User>) query.getResultList();
    }

    public Long countAllUsersByUsername(String value) {
	Query query = entityManager.createNamedQuery(
		User.COUNT_ALL_USERS_BY_NAME).setParameter(1, value);
	return (Long) query.getSingleResult();
    }

    public List<User> findUserByFirstname(String value, int firstItemOmRow,
	    int itemsPerPage) {
	Query query = entityManager.createNamedQuery(
		User.FIND_USER_BY_FIRSTNAME).setParameter(1, value);
	query = query.setFirstResult(firstItemOmRow)
		.setMaxResults(itemsPerPage);
	return (List<User>) query.getResultList();
    }

    public Long countAllUsersByFirstname(String value) {
	Query query = entityManager.createNamedQuery(
		User.COUNT_ALL_USERS_BY_FIRSTNAME).setParameter(1, value);
	return (Long) query.getSingleResult();
    }

    public List<User> findUserByLastname(String value, int firstItemOmRow,
	    int itemsPerPage) {
	Query query = entityManager
		.createNamedQuery(User.FIND_USER_BY_LASTNAME).setParameter(1,
			value);
	query = query.setFirstResult(firstItemOmRow)
		.setMaxResults(itemsPerPage);
	return (List<User>) query.getResultList();
    }

    public Long countAllUsersByLastname(String value) {
	Query query = entityManager.createNamedQuery(
		User.COUNT_ALL_USERS_BY_LASTNAME).setParameter(1, value);
	return (Long) query.getSingleResult();
    }

    public List<User> findUserByEmail(String value, int firstItemOmRow,
	    int itemsPerPage) {
	Query query = entityManager.createNamedQuery(User.FIND_USER_BY_EMAIL)
		.setParameter(1, value);
	query = query.setFirstResult(firstItemOmRow)
		.setMaxResults(itemsPerPage);
	return (List<User>) query.getResultList();
    }

    public Long countAllUsersByEmail(String value) {
	Query query = entityManager.createNamedQuery(
		User.COUNT_ALL_USERS_BY_EMAIL).setParameter(1, value);
	return (Long) query.getSingleResult();
    }
}
