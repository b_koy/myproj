package com.my.pro.service;

import com.my.pro.entity.User;
import com.my.pro.exceptions.UserServiceException;

public interface UserService {

    public boolean makeUserAdminByName(String username) throws UserServiceException;

    public boolean createUser(String username, String firstname,
	    String lastname, String email, String password) throws UserServiceException;

    public User findUserByName(String username);

}
