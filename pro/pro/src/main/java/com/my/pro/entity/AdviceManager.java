package com.my.pro.entity;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "advice_manager")
public class AdviceManager {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "aManagerId", nullable = false)
    private long aManagerId;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "userId", nullable = false)
    private User user;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "adviceManager",cascade = CascadeType.ALL)
    private Set<Advice> advices;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "adviceManager", cascade = CascadeType.ALL)
    private Set<Comment> comments;

    @Column(name = "adviceCounter", nullable = true)
    private long adviceCounter;

    /**
     * @return the adviceCounter
     */
    public long getAdviceCounter() {
	return adviceCounter;
    }

    /**
     * @param adviceCounter
     *            the adviceCounter to set
     */
    public void setAdviceCounter(long adviceCounter) {
	this.adviceCounter = adviceCounter;
    }

    /**
     * 
     */
    public AdviceManager() {
	super();
    }

    /**
     * @return the aManagerId
     */
    public long getaManagerId() {
	return aManagerId;
    }

    /**
     * @param aManagerId
     *            the aManagerId to set
     */
    public void setaManagerId(long aManagerId) {
	this.aManagerId = aManagerId;
    }

    /**
     * @return the user
     */
    public User getUser() {
	return user;
    }

    /**
     * @param user
     *            the user to set
     */
    public void setUser(User user) {
	this.user = user;
    }

    /**
     * @return the advices
     */
    public Set<Advice> getAdvices() {
	return advices;
    }

    /**
     * @param advices
     *            the advices to set
     */
    public void setAdvices(Set<Advice> advices) {
	this.advices = advices;
    }

    /**
     * @return the comments
     */
    public Set<Comment> getComments() {
	return comments;
    }

    /**
     * @param comments
     *            the comments to set
     */
    public void setComments(Set<Comment> comments) {
	this.comments = comments;
    }

   
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return "AdviceManager [aManagerId=" + aManagerId + ", advices="
		+ advices + ", comments=" + comments + ", adviceCounter="
		+ adviceCounter + "]";
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + (int) (aManagerId ^ (aManagerId >>> 32));
	result = prime * result
		+ (int) (adviceCounter ^ (adviceCounter >>> 32));
	return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	AdviceManager other = (AdviceManager) obj;
	if (aManagerId != other.aManagerId)
	    return false;
	if (adviceCounter != other.adviceCounter)
	    return false;
	return true;
    }

    /**
     * Add advice to AdviceManager
     * 
     * @param advice
     * @return
     */
    public boolean addAdviceToManager(Advice advice) {
	if (advices == null)
	    advices = new LinkedHashSet<>();
	advice.setAdviceNumberInManager(++adviceCounter);	
	advices.add(advice);
	return true;
    }

    /**
     * Add advice Set to AdviceManager
     * 
     * @param adviceSet
     * @return
     */
    public boolean addAdviceSetToManager(Set<Advice> adviceSet) {
	if (advices == null)
	    advices = new LinkedHashSet<>();
	    
	for(Advice advice : adviceSet){
	    advice.setAdviceNumberInManager(++adviceCounter);
	}
	advices.addAll(adviceSet);
//	adviceCounter += adviceSet.size();
	return true;
    }

}
