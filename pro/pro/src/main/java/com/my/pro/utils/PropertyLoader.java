package com.my.pro.utils;

import java.io.IOException;
import java.util.Properties;


/**
 * Load properies from file
 * @author bkoles
 *
 */
public class PropertyLoader { 
    private static final String PROP_FILE = "/application.properties";
    private static Properties props;

    private PropertyLoader() {
    }

    public static String loadProperty(String name) {
	if (props == null) {
	    props = new Properties();
	    try {
		props.load(PropertyLoader.class.getResourceAsStream(PROP_FILE));
	    } catch (IOException e) {
		e.printStackTrace();
	    }
	}
	String value = "";

	if (name != null) {
	    value = props.getProperty(name);
	}
	return value;
    }
}