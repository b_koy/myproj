package com.my.pro.service.implementation;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.my.pro.dao.CityDao;
import com.my.pro.dao.CountryDao;
import com.my.pro.dao.UserDao;
import com.my.pro.entity.City;
import com.my.pro.entity.Country;
import com.my.pro.entity.Role;
import com.my.pro.entity.User;
import com.my.pro.exceptions.AdminPanelServiceException;
import com.my.pro.exceptions.CityDownloaderException;
import com.my.pro.service.AdminPanelService;
import com.my.pro.utils.PropertyLoader;
import com.my.pro.utils.cityDownloader;

/**
 * Admin panel service
 * 
 * @author bkoles
 * 
 */
@Service
public class AdminPanelServiceImpl implements AdminPanelService {
    private static final Logger LOGGER = Logger
	    .getLogger(AdminPanelServiceImpl.class);
    private static final int COUNT_OF_CITIES_FOR_BIG_COUNTRY = Integer
	    .parseInt(PropertyLoader
		    .loadProperty("adminPanelService.cityCount"));

    @Autowired
    CountryDao countryDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private CityDao cityDao;

    @Transactional
    public List<User> findAllUsers() {
	LOGGER.info("Return list All users");
	return userDao.findAllUsers();
    }

    @Transactional
    public List<User> findAllUsersRange(int pageNum, int itemsOnPage) {
	LOGGER.info("Return list All users by range "
		+ (pageNum * itemsOnPage - itemsOnPage) + " - " + itemsOnPage);
	return userDao.findAllUsersRange((pageNum * itemsOnPage - itemsOnPage),
		itemsOnPage);

    }

    @Transactional
    public List<Role> findAllRoles() {
	LOGGER.info("Return list All roles");
	return userDao.findAllRoles();
    }

    @Transactional
    public Role findRoleByName(String name) {
	LOGGER.info("Return role " + name);
	return userDao.findRoleByName(name);
    }

    @Transactional
    public boolean removeUser(String username)
	    throws AdminPanelServiceException {
	User user = userDao.findByName(username);
	try {

	    if (user != null) {
		userDao.remove(user);
		return true;

	    } else {
		return false;
	    }

	} catch (Exception e) {
	    // LOGGER.error(e);
	    throw new AdminPanelServiceException("Could not remove user", e);
	}
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
    public boolean updateUser(User user) throws AdminPanelServiceException {
	try {
	    userDao.update(user);
	    return true;
	} catch (Exception e) {
	    // LOGGER.error(e);
	    throw new AdminPanelServiceException("Could not update user", e);
	}
    }

    @Transactional
    public Long countAllUsers() {
	return userDao.countAllUsers();
    }

    @Transactional
    public List<User> findUserByField(String field, String fieldValue,
	    int firstItemOmRow, int itemsPerPage) {
	int page = firstItemOmRow * itemsPerPage - itemsPerPage;
	switch (field) {
	case "username":
	    return userDao.findUserByUsername(fieldValue, page, itemsPerPage);
	case "id":
	    return userDao.findUserById(fieldValue, page, itemsPerPage);
	case "firstname":
	    return userDao.findUserByFirstname(fieldValue, page, itemsPerPage);
	case "lastname":
	    return userDao.findUserByLastname(fieldValue, page, itemsPerPage);
	case "email":
	    return userDao.findUserByEmail(fieldValue, page, itemsPerPage);
	default:
	    return null;
	}

    }

    @Transactional
    public Long findCountUsersByField(String field, String fieldValue) {
	switch (field) {
	case "username":
	    return userDao.countAllUsersByUsername(fieldValue);
	case "id":
	    return userDao.countAllUsersById(fieldValue);
	case "firstname":
	    return userDao.countAllUsersByFirstname(fieldValue);
	case "lastname":
	    return userDao.countAllUsersByLastname(fieldValue);
	case "email":
	    return userDao.countAllUsersByEmail(fieldValue);
	default:
	    return null;
	}

    }

    @Transactional
    @Override
    public boolean downloadAllCities() throws CityDownloaderException {
	LOGGER.info("User manager - Start downloading all cities");
	boolean boo = cityDownloader.downloadAllCities(countryDao
		.findAllCountries());
	LOGGER.info("User manager - Finish downloading all cities");
	return boo;

    }

    @Transactional
    @Override
    public boolean updateCitiesForCountry(Country country)
	    throws AdminPanelServiceException {
	LOGGER.info("User manager - Start updating cities for "
		+ country.getCountryName());
	try {
	    Set<String> cityStringList = cityDownloader
		    .findCitiesByCountryCode(country.getCountryCode());
	    if (cityStringList != null) {
		if (cityStringList.size() > COUNT_OF_CITIES_FOR_BIG_COUNTRY) {
		    List<String> list = new ArrayList<>();
		    list.addAll(cityStringList);
		    int fromIndex = 0;
		    int toIndex = COUNT_OF_CITIES_FOR_BIG_COUNTRY;
		    while (fromIndex + 1 < list.size()) {
			Set<String> tmpList = new LinkedHashSet<String>(
				list.subList(fromIndex, toIndex));
			country = countryCitiesUpdate(country, tmpList);
			fromIndex = toIndex;
			if ((toIndex + COUNT_OF_CITIES_FOR_BIG_COUNTRY) > list
				.size()) {
			    toIndex = list.size();
			} else {
			    toIndex = toIndex + COUNT_OF_CITIES_FOR_BIG_COUNTRY;
			}
		    }
		} else {
		    country = countryCitiesUpdate(country, cityStringList);
		}
	    }
	    LOGGER.info("User manager - Finish updating cities for "
		    + country.getCountryName() + ". Updated "
		    + (cityStringList != null ? cityStringList.size() : 0)
		    + " cities.");
	    return true;
	} catch (CityDownloaderException e) {
	    throw new AdminPanelServiceException("Can't update cities for "
		    + country.getCountryName(), e);
	}
    }

    private Country countryCitiesUpdate(Country country,
	    Set<String> cityStringList) {
	country = countryDao.findCountryByCountryName(country.getCountryName());
	Set<City> citySet = country.getCities();
	for (String cityString : cityStringList) {
	    City city = new City();
	    city.setCityName(cityString);
	    city.setCountry(country);
	    citySet.add(city);
	}
	country.setCities(citySet);
	countryDao.updateCountry(country);
	return country;
    }

    @Transactional
    @Override
    public List<Country> findAllCountries() {
	return countryDao.findAllCountries();
    }

    @Transactional
    @Override
    public Country findCountryByName(String countryName) {
	return countryDao.findCountryByCountryName(countryName);
    }
}
