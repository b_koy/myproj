/**
 * 
 */
var countryList;
var cityList;

$(document).ready(function() {

	$("#usercountry").focus(function() {
		$.ajax({
			type : "GET",
			url : "secure_userProfile?action=getCountryList",
			success : function(response) {
				countryList = response.split(':');
				$("#usercountry").autocomplete({
					source : countryList,
					change : function(event, ui) {
						changeCityInput();
					}
				});
			},
			error : function(e) {
				alert('Error: ' + e);
			}
		});
	});

	$("#usercity").focus(function() {
		$.ajax({
			type : "POST",
			data : {
				"countryName" : $("#usercountry").val(),
			},
			url : "secure_userProfile?action=getCityList",
			success : function(response) {
				cityList = response.split(':');
				$("#usercity").autocomplete({
					source : cityList,
					minLength : 2,
					change : function(event, ui) {
						changeStreetZipcodeInput();
					}
				});
			},
			error : function(e) {
				alert('Error: ' + e);
			}
		});

	});

	$('#usercountry').change(function() {
		changeCityInput();
	});

	$('#usercity').change(function() {
		changeStreetZipcodeInput();
	});
	
	$('#changePasswordMenu').click(function(){
		if ($('#changePasword').is(':visible')){
			$('#changePasword').hide();
		} else{
			$('#changePasword').show();
		}		
	});
	
	changeCityInput();
	changeStreetZipcodeInput();
});

function updateUserProfile() {
	$.ajax({
		type : "POST",
		data : {
			"username" : $("#currentUsername").html(),
			"firstname" : $("#firstname").val(),
			"lastname" : $("#lastname").val(),
			"email" : $("#email").val(),
			"skype" : $("#skype").val(),
			"phonenumber" : $("#phonenumber").val(),
			"usercountry" : $("#usercountry").val(),
			"usercity" : $("#usercity").val(),
			"userstreet" : $("#userstreet").val(),
			"zipcode" : $("#zipcode").val(),
			"newpass" : $("#newpass").val(),
		},
		url : "secure_userProfile?action=updateUserProfile",
		success : function(response) {
			$('body').html(response);
		},
		error : function(e) {
			alert('Error: ' + e);
		}
	});
}

function changeCityInput() {
	if ($('#usercountry').val().length != 0) {
		$('#usercity').attr('disabled', false);
	} else {
		$('#usercity').val('');
		$('#usercity').attr('disabled', true);
		$('#userstreet').val('');
		$('#userstreet').attr('disabled', true);
		$('#zipcode').val('');
		$('#zipcode').attr('disabled', true);
	}
}

function changeStreetZipcodeInput() {
	if ($('#usercity').val().length != 0) {
		$('#userstreet').attr('disabled', false);
		$('#zipcode').attr('disabled', false);
	} else {
		$('#userstreet').val('');
		$('#userstreet').attr('disabled', true);
		$('#zipcode').val('');
		$('#zipcode').attr('disabled', true);
	}
}
