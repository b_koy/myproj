$(document).ready(function() {

	$('body').append('<div id="blackout"></div>');

	var boxWidth = 400;

	function centerBox() {

		/* Preliminary information */
		var winWidth = $(window).width();
		var winHeight = $(document).height();
		var scrollPos = $(window).scrollTop();
		/* auto scroll bug */

		/* Calculate positions */

		var disWidth = (winWidth - boxWidth) / 2
		var disHeight = scrollPos + 150;

		/* Move stuff about */
		$('.popup-box').css({
			'width' : boxWidth + 'px',
			'left' : disWidth + 'px',
			'top' : disHeight + 'px'
		});
		$('#blackout').css({
			'width' : winWidth + 'px',
			'height' : winHeight + 'px'
		});

		return false;
	}

	$(window).resize(centerBox);
	$(window).scroll(centerBox);
	centerBox();

	$('[class*=popup-link]').click(function(e) {

		/* Prevent default actions */
		e.preventDefault();
		e.stopPropagation();

		/* Get the id (the number appended to the end of the classes) */
		var name = $(this).attr('class');
		var id = name[name.length - 1];
		var scrollPos = $(window).scrollTop();

		/* Show the correct popup box, show the blackout and disable scrolling */
		$('#popup-box-' + id).show();
		$('#blackout').show();
		$('html,body').css('overflow', 'hidden');

		/* Fixes a bug in Firefox */
		$('html').scrollTop(scrollPos);
	});
	$('[class*=popup-box]').click(function(e) {
		/* Stop the link working normally on click if it's linked to a popup */
		e.stopPropagation();
	});
	$('html').click(function() {
		var scrollPos = $(window).scrollTop();
		/* Hide the popup and blackout when clicking outside the popup */
		$('[id^=popup-box-]').hide();
		$('#blackout').hide();
		$("html,body").css("overflow", "auto");
		$('html').scrollTop(scrollPos);
	});
	$('.close').click(function() {
		var scrollPos = $(window).scrollTop();
		/* Similarly, hide the popup and blackout when the user clicks close */
		$('[id^=popup-box-]').hide();
		$('#blackout').hide();
		$("html,body").css("overflow", "auto");
		$('html').scrollTop(scrollPos);
	});

	// Get file path
	$("#upload").change(function() {
		var filename = $(this).val();
		$('#fileName').val(filename);
	}).change();

	// Getting advice page
	$('.trAdviceTable').on('click', function() {
		var adviceId = $(this).siblings("#adviceId").val();
		var username = $('#currentUsername').text();
		alert(adviceId + username);
		$.get("secure_advice?action=getAdvicePage", {
			adviceId : adviceId,
			username : username
		});
	});

});
// Geting file from form
function uploadJqueryForm() {
	$('#result').html('');

	$("#form").ajaxForm({
		success : function(data) {
			$('#result').html(data);
		},
		dataType : "text"
	}).submit();
}

// Don't used
function uploadFormData(username) {
	$('#result').html('');

	var oMyForm = new FormData();
	oMyForm.append("file", file2.files[0]);

	$.ajax({
		url : 'secure_userProfile?action=uploadAvatar',
		data : oMyForm,
		dataType : 'text',
		processData : false,
		contentType : false,
		type : 'POST',
		success : function(data) {
			$('#result').html(data);
		}
	});
}

function removeAdvice(adviceId) {
	if (confirm("Are you sure want delete advice?")) {
		$.ajax({
			type : 'POST',
			url : 'secure_advice?action=removeAdvice',
			data : {
				'adviceId' : adviceId,
				'username' : $('#currentUsername').text(),
			},
			success : function(data) {
				$('body').html(data);
			}
		})
	} else {
	}

}
