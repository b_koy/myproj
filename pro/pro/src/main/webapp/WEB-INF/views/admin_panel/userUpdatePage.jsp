<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<link href="<c:url value="/resources/css/validation.css" />"
	rel="stylesheet" type="text/css" />
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery-1.5.2.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/userUpdateValidate.js" />"></script>
<c:set var="ROLE_USER" value="ROLE_USER"></c:set>
<form id="userUpdateForm">
	<fieldset>
		<legend style="color: green" align="left">
			<strong>Update User</strong>
		</legend>
		<div align="left">
			<div align="left">User name:</div>
			<input class="input-large" type="text" id="user_name" name="userName"
				value="${user.username}" readonly />
			<div align="left">First name:</div>
			<input class="input-large" type="text" id="first_name"
				name="firstName" value="${user.firstname}" />
			<div align="left">Last name:</div>
			<input class="input-large" type="text" id="last_name" name="lastName"
				value="${user.lastname}" />
			<div align="left">E-mail:</div>
			<input class="input-large" type="text" id="e-mail" name="email"
				value="${user.email}" />
			<div align="left">Password:</div>
			<input class="input-large" type="text" id="pass" name="password"
				value="${user.password}" />
			<div id="userRoles">
				<p>Roles:</p>
				<c:forEach var="role" items="${userRolesMap}">
					<c:if test="${role.value == true}">
						<c:choose>
							<c:when test="${role.key == ROLE_USER}">
								<INPUT NAME="options" id="userRoles" TYPE="CHECKBOX"
									VALUE="${role.key}" disabled="disabled" checked> ${role.key} <br>
							</c:when>
							<c:otherwise>
								<INPUT NAME="options" id="userRoles" TYPE="CHECKBOX"
									VALUE="${role.key}" checked> ${role.key} <br>
							</c:otherwise>
						</c:choose>
					</c:if>
					<c:if test="${role.value == false}">
						<INPUT NAME="options" id="userRoles" TYPE="CHECKBOX"
							VALUE="${role.key}"> ${role.key} <br>
					</c:if>
				</c:forEach>
			</div>
		</div>
	</fieldset>
	<input id="btn" class="btn btn-primary" type="button" value="Update" />
</form>
