<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<script type="text/javascript"
	src="/pro/resources/js/jquery.simplePagination.js"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/adminPanel.js" />"></script>
<link href="<c:url value="/resources/css/adminPanel.css" />"
	rel="stylesheet" type="text/css" />

	
</head>
<body>
	<div id="userManagerMenu">
		<h4 style="text-align: center">User maneger</h4>
		<div class="row">
			<div class="btn-group">
				<button class="btn btn-default" onclick="findAllUsers(1,10)">Load
					user list</button>
				<button class="btn btn-default" onclick="AddNewUserPage()">Add
					new user</button>
				<button class="btn btn-default" onclick="downloadCities()">Download
					and update cities</button>
			</div>
		</div>
		<div class="row">
			<div id="findBlock">
				<button id="findButton" onclick="findUserByField(1,10)"
					class="btn btn-default">Find</button>
				<select id="findBySelect" class="form-control">
					<option value="userId">by ID</option>
					<option value="username">by User Name</option>
					<option value="firstname">by First Name</option>
					<option value="lastname">by Last Name</option>
					<option value="email">by E-mail</option>
				</select> <input id="findUserByInput" class="form-control" value="Find user"
					onfocus="this.value=''">
			</div>
		</div>
	</div>
	<div id="ajax_indicator">
		<img src="/pro/resources/images/ajax-loader.gif" />
	</div>
	<div id="popUpBackground"></div>
	<div id="userManagerResult"></div>

</body>
</html>