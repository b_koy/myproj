<%@ page language="java" contentType="text/html; charset=UTF8"
	pageEncoding="UTF8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page session="false"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<c:url value="/resources/css/index.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet" type="text/css" />




<title><tiles:insertAttribute name="title" ignore="true" /></title>
</head>
<body>
	<div class="wrapper">
		<div class="row">
			<div class="col-md-12" id="header">
				<tiles:insertAttribute name="header" />
			</div>
		</div>

		<div class="row">
			<div class="col-md-12" id="menu">
				<tiles:insertAttribute name="menu" />
			</div>
		</div>
		<div class="container bs-docs-container">
			<div class="row">
				<div class="col-md-12" id="body">
					<tiles:insertAttribute name="body" />
				</div>
			</div>
		</div>
	</div>
	<div class="footer">
		<div class="row">
			<tiles:insertAttribute name="footer" />
		</div>
	</div>
</body>
</html>
