<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<div class="siteName">ADVICER</div>
<div class="singIn">
	<div>
		<sec:authorize access="isAnonymous()">
				Welcome! Guest		
			</sec:authorize>
		<sec:authorize access="isAuthenticated()">
				Welcome <span id="currentUsername"><sec:authentication
					property="principal.username" /></span>

		</sec:authorize>
	</div>
</div>
