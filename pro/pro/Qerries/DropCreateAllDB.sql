SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `myprodb` ;
CREATE SCHEMA IF NOT EXISTS `myprodb` DEFAULT CHARACTER SET utf8 ;
USE `myprodb` ;

-- -----------------------------------------------------
-- Table `myprodb`.`countries`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `myprodb`.`countries` ;

CREATE TABLE IF NOT EXISTS `myprodb`.`countries` (
  `countryId` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `countryCode` VARCHAR(255) NOT NULL,
  `countryName` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`countryId`))
ENGINE = InnoDB
AUTO_INCREMENT = 264
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `myprodb`.`cities`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `myprodb`.`cities` ;

CREATE TABLE IF NOT EXISTS `myprodb`.`cities` (
  `cityId` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `cityName` VARCHAR(255) NULL DEFAULT NULL,
  `countryId` BIGINT(20) NOT NULL,
  PRIMARY KEY (`cityId`),
  INDEX `FKAEEDBB4957002BD2` (`countryId` ASC),
  CONSTRAINT `FKAEEDBB4957002BD2`
    FOREIGN KEY (`countryId`)
    REFERENCES `myprodb`.`countries` (`countryId`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `myprodb`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `myprodb`.`users` ;

CREATE TABLE IF NOT EXISTS `myprodb`.`users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `ctime` DATETIME NULL DEFAULT NULL,
  `email` VARCHAR(255) NOT NULL,
  `firstname` VARCHAR(255) NULL DEFAULT NULL,
  `lastname` VARCHAR(255) NULL DEFAULT NULL,
  `ltime` DATETIME NULL DEFAULT NULL,
  `password` VARCHAR(255) NOT NULL,
  `username` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username` (`username` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `myprodb`.`streets`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `myprodb`.`streets` ;

CREATE TABLE IF NOT EXISTS `myprodb`.`streets` (
  `streetId` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `apartmentNumber` VARCHAR(255) NULL DEFAULT NULL,
  `buildingNumber` VARCHAR(255) NULL DEFAULT NULL,
  `streetName` VARCHAR(255) NULL DEFAULT NULL,
  `zipCode` VARCHAR(255) NULL DEFAULT NULL,
  `cityId` BIGINT(20) NOT NULL,
  PRIMARY KEY (`streetId`),
  INDEX `FK8FD4B0D01534A806` (`cityId` ASC),
  CONSTRAINT `FK8FD4B0D01534A806`
    FOREIGN KEY (`cityId`)
    REFERENCES `myprodb`.`cities` (`cityId`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `myprodb`.`contacts`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `myprodb`.`contacts` ;

CREATE TABLE IF NOT EXISTS `myprodb`.`contacts` (
  `contactId` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `phoneNumber` VARCHAR(255) NULL DEFAULT NULL,
  `site` VARCHAR(255) NULL DEFAULT NULL,
  `skypeName` VARCHAR(255) NULL DEFAULT NULL,
  `cityId` BIGINT(20) NULL DEFAULT NULL,
  `countryId` BIGINT(20) NULL DEFAULT NULL,
  `streetId` BIGINT(20) NULL DEFAULT NULL,
  `userId` INT(11) NOT NULL,
  PRIMARY KEY (`contactId`),
  INDEX `FKDE2D605357002BD2` (`countryId` ASC),
  INDEX `FKDE2D605376046DF6` (`streetId` ASC),
  INDEX `FKDE2D60531534A806` (`cityId` ASC),
  INDEX `FKDE2D6053347A34C6` (`userId` ASC),
  CONSTRAINT `FKDE2D6053347A34C6`
    FOREIGN KEY (`userId`)
    REFERENCES `myprodb`.`users` (`id`),
  CONSTRAINT `FKDE2D60531534A806`
    FOREIGN KEY (`cityId`)
    REFERENCES `myprodb`.`cities` (`cityId`),
  CONSTRAINT `FKDE2D605357002BD2`
    FOREIGN KEY (`countryId`)
    REFERENCES `myprodb`.`countries` (`countryId`),
  CONSTRAINT `FKDE2D605376046DF6`
    FOREIGN KEY (`streetId`)
    REFERENCES `myprodb`.`streets` (`streetId`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `myprodb`.`hibernate_sequence`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `myprodb`.`hibernate_sequence` ;

CREATE TABLE IF NOT EXISTS `myprodb`.`hibernate_sequence` (
  `next_val` BIGINT(20) NULL DEFAULT NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `myprodb`.`roles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `myprodb`.`roles` ;

CREATE TABLE IF NOT EXISTS `myprodb`.`roles` (
  `role_id` INT(11) NOT NULL,
  `role` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`role_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `myprodb`.`user_roles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `myprodb`.`user_roles` ;

CREATE TABLE IF NOT EXISTS `myprodb`.`user_roles` (
  `user_id` INT(11) NOT NULL,
  `role_id` INT(11) NOT NULL,
  PRIMARY KEY (`user_id`, `role_id`),
  INDEX `FK73429949B85F2CCF` (`role_id` ASC),
  INDEX `FK734299495D89F0AF` (`user_id` ASC),
  CONSTRAINT `FK734299495D89F0AF`
    FOREIGN KEY (`user_id`)
    REFERENCES `myprodb`.`users` (`id`),
  CONSTRAINT `FK73429949B85F2CCF`
    FOREIGN KEY (`role_id`)
    REFERENCES `myprodb`.`roles` (`role_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
