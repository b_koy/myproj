INSERT INTO `myprodb`.`users` (`id`, `email`, `firstname`, `lastname`, `password`, `username`) VALUES ('1', 'admin@pro.com', 'admin', 'admin', '12345678', 'admin');
INSERT INTO `myprodb`.`roles` (`role_id`, `role`) VALUES ('1', 'ROLE_ADMIN');
INSERT INTO `myprodb`.`roles` (`role_id`, `role`) VALUES ('2', 'ROLE_USER');
INSERT INTO `myprodb`.`user_roles` (`user_id`, `role_id`) VALUES ('1', '1');
INSERT INTO `myprodb`.`user_roles` (`user_id`, `role_id`) VALUES ('1', '2');
