<%@ page language="java" contentType="text/html; charset=UTF8"
	pageEncoding="UTF8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="true"%>
<script type="text/javascript"
	src="/pro/resources/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript"
	src="/pro/resources/js/jquery.simplePagination.js"></script>
<script type="text/javascript" src="/pro/resources/js/adminPanel.js"></script>

<link href="<c:url value="/resources/css/result.css" />"
	rel="stylesheet" type="text/css" />
<link href="<c:url value="/resources/css/simplePagination.css" />"
	rel="stylesheet" type="text/css" />
<link href="<c:url value="/resources/css/adminPanel.css" />"
	rel="stylesheet" type="text/css" />

<div id="resultMessage">
	<c:if test="${!empty successMsg}">
		<p align="left" style="color: green;">${successMsg}</p>
	</c:if>
	<c:if test="${!empty incorrectMsg}">
		<p align="left" style="color: red">${incorrectMsg}</p>
	</c:if>
	<c:if test="${!empty errorMsg}">
		<p align="left" style="color: red;">${errorMsg}</p>
		<div class="my-item">
			<div class="fulltext">
				<c:forEach var="error" items="${errorList}">
					<p>${error}</p>
				</c:forEach>
			</div>
		</div>
	</c:if>
</div>
<div id="totalUserInfo">
	Loaded users - <span id="allUsersCount">${allUsersCount} </span> <br>
	Users per page - <select id="itemsPerPage">
		<option value="10">10</option>
		<option value="25">25</option>
		<option value="50">50</option>
		<option value="100">100</option>
		<option value="${allUsersCount}">All</option>
	</select>
</div>
<table class="table table-hover">
	<tr>
		<td>User ID</td>
		<td>Username</td>
		<td>Password</td>
		<td>Firstname</td>
		<td>Lastname</td>
		<td>E-mail</td>
		<td>Roles</td>
		<td>Change</td>
	</tr>
	<c:forEach var="user" items="${userList}">
		<tr>
			<td>${user.userId}</td>
			<td>${user.username}</td>
			<td>${user.password}</td>
			<td>${user.firstname}</td>
			<td>${user.lastname}</td>
			<td>${user.email}</td>
			<td><c:forEach var="role" items="${user.roleSet}">
					${role} <br>
				</c:forEach></td>
			<td><a href="#" onclick="userUpdateGetPage('${user.username}')">Edit</a>
				<a href="#" onclick="removeUser('${user.username}')">Delete</a></td>
		</tr>
	</c:forEach>
</table>
<div class="pagenationWrapper">
	<div id="userManagerPagenation"></div>
</div>


