<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page session="true"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<title>Login</title>

<div class="centerWrapper">
	<div class="centerForm">
		<div class="row">
			<div class="col-sm-offset-2 col-sm-5">
				<h4>Login</h4>
			</div>
		</div>
		<form class="form-horizontal" role="form"
			action="j_spring_security_check" method="post">
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Login
					name</label>
				<div class="col-sm-5">
					<input type="text" class="form-control" id="inputLogin"
						name="j_username" placeholder="login">
				</div>
			</div>
			<div class="form-group">
				<label for="inputPassword3" class="col-sm-3 control-label">Password</label>
				<div class="col-sm-5">
					<input type="password" class="form-control" id="inputPassword3"
						name="j_password" placeholder="Password">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-5">
					<div class="checkbox">
						<label> <input type="checkbox"> Remember me
						</label>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-5">
					<button type="submit" class="btn btn-default">Sign in</button>
				</div>
			</div>
		</form>
	</div>
</div>