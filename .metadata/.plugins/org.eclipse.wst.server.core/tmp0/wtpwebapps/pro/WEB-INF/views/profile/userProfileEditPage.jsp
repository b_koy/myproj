<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript"
	src="<c:url value="/resources/js/profilePageEdit.js" />"></script>
<title>Insert title here</title>
</head>
<body>
	<div>
		First name: <input id="firstname" value="${user.firstname}"><br>
		Last name: <input id="lastname" value="${user.lastname}"><br>
		E-mail: <input id="email" value="${user.email}"><br>
		Skype: <input id="skype" value="${user.contact.skypeName}"><br>
		Phone number: <input id="phonenumber"
			value="${user.contact.phoneNumber}"><br> Country: <input
			id="usercountry" value="${user.contact.country.countryName}">
		<br>City: <input id="usercity"
			value="${user.contact.city.cityName}"> <br> Street: <input
			id="userstreet" value="${user.contact.streetName}"> <br>
		Zip-code: <input id="zipcode" value="${user.contact.zipcode}"><br>
		<div id="changePasswordMenu" style="font-size: small; cursor: pointer;">change password</div>
		<div id="changePasword" style="display: none;">
			New password:<input id="newpass" value=""><br> 
			Repeat new password:<input id="newpassrepeat" value=""><br>
		</div>

		<button onclick="updateUserProfile()">Update</button>
	</div>
</body>
</html>