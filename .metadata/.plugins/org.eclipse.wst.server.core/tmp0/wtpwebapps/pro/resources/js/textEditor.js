/**
 * 
 */
var currentUsername
$(document).ready(function() {
	
	currentUsername = $("#currentUsername").text();
	// Check page type
	if ($('#pageType').val() === 'advicePage') {
		// Checking creator of advice
		checkCreator();
	} else {
		$('#summernote').summernote();
		$("#saveButton").show();
		$("#editButton").hide();

	}

	// Saves advice
	$("#saveButton").on("click", function() {
		if ($('#pageType').val() === 'advicePage') {
			updateAdvice();
			// $('#adviceNameDiv').html($("#adviceName").val());
			// $("#adviceNameDiv").show();
			// $("#adviceName").hide();
		} else {
			saveAdvice();
		}
		// $('#summernote').destroy();
		// $("#saveButton").hide();
		// if ($("#editButton").length) {
		// $("#editButton").show();
		// }
	});

	// Edit advice
	$("#editButton").on("click", function() {
		$('#summernote').summernote();
		$("#saveButton").show();
		$("#editButton").hide();
		$("#adviceName").show();
		$("#adviceNameDiv").hide();
	});

});

function saveAdvice() {
	$.ajax({
		type : "POST",
		url : "secure_advice?action=addAdvice",
		data : {
			"adviceText" : $('#summernote').code(),
			"username" : currentUsername,
			"adviceName" : $("#adviceName").val(),
		},
		success : function(response) {
			$('body').html(response);
//			setTimeout(function() {
//				$.get("secure_userProfile?action=getProfilePage",{username: currentUsername});
//			}, 100);
		},
		error : function(e) {
			alert('Error: ' + e);
		}
	});
}

function updateAdvice() {
	$.ajax({
		type : "POST",
		url : "secure_advice?action=updateAdvice",
		data : {
			"adviceId" : $('#adviceId').val(),
			"adviceText" : $('#summernote').code(),
			"username" : currentUsername,
			"adviceName" : $("#adviceName").val()
		},
		success : function(response) {
			$('body').html(response);
//			setTimeout(function() {
//				$.get("secure_userProfile?action=getProfilePage",{username: currentUsername});
//			}, 100);
		},
		error : function(e) {
			alert('Error: ' + e);
		}
	});
}

// checks if current user is creator of advice
function checkCreator() {
	if ($('#adviceCreator').val() != $('#adviceCreator').val()) {
		$("#saveButton").remove();
		$("#editButton").remove();
	} else {
		$("#saveButton").hide();
		$("#editButton").show();
	}
}
