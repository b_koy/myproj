/**
 * 
 */
$(document).ready(function() {
	// Cache the elements we'll need
	var menu = $('#cssmenu');
	var menuList = menu.find('ul:first');
	var listItems = menu.find('li').not('#responsive-tab');

	// Create responsive trigger
	menuList.prepend('<li id="responsive-tab"><a href="#">Menu</a></li>');

	// Toggle menu visibility
	menu.on('click', '#responsive-tab', function(){
		listItems.slideToggle('fast');
		listItems.addClass('collapsed');
	});
	
});

function getUserProfilePage() {
	$.ajax({
		type : "POST",
		url : "userProfile?action=getProfilePage",
		data : {
			"username" : $('#currentUsername').html(),
		},
		success : function(response) {
			$('#body').html(response);
		},
		error : function(e) {
			alert('Error: ' + e);
		}
	});
}

function getUserProfileEditPage() {
	$.ajax({
		type : "POST",
		url : "userProfile?action=getProfileEditPage",
		data : {
			"username" : $('#currentUsername').html(),
		},
		success : function(response) {
			$('#body').html(response);
		},
		error : function(e) {
			alert('Error: ' + e);
		}
	});
}