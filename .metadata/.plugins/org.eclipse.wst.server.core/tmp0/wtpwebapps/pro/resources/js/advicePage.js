/**
 * 
 */
$(document).ready(function() {
	$(".addCommentForm").validate({
		rules : {
			commentText : {
				required : true,
			},
		},
		messages : {
			commentText : {
				required : "Please input some commnet",
			},

		},

	});

	$('#addComment').click(function() {
		if ($(".addCommentForm").valid()) {
			$.post("secure_advice?action=addcommentToAdvice", {
				adviceId : $('#adviceId').val(),
				commentText : $('#commentText').val(),
				username: $('#currentUsername').text(),
			}).done(function( data ) {
				$('html').html(data);
			});
		}
	});
	
});

function removeCommentById(commentId){
	if (confirm('Are you sure want remove this comment?')) {
		$.post("secure_advice?action=removeCommentById", {
			adviceId : $('#adviceId').val(),
			commentId : commentId,
		}).done(function( data ) {
			$('html').html(data);
		});
	}
}