package com.my.pro.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.my.pro.entity.Advice;
import com.my.pro.exceptions.AdviceServiceException;
import com.my.pro.service.AdviceService;
import com.my.pro.service.UserService;

/**
 * Controller for managing advices
 * 
 * @author bkoles
 * 
 */
@Controller
@RequestMapping("secure_advice")
public class AdviceController {
    private static final Logger LOGGER = LoggerFactory
	    .getLogger(AdviceController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private AdviceService adviceService;

    /**
     * Return add advice page
     * 
     * @return
     */
    @RequestMapping(params = "action=getAddAdvicePage", method = RequestMethod.GET)
    public String getProfilePage() {
	LOGGER.info("Add advice page");
	return "user_addAdvicePage";
    }

    /**
     * Add advice to user and save it on server
     * 
     * @param html
     * @param username
     * @param advicename
     * @return
     */
    @RequestMapping(params = "action=addAdvice", method = RequestMethod.POST)
    @ResponseBody
    public String addAdviceForUser(
	    @ModelAttribute(value = "adviceId") String adviceId,
	    @ModelAttribute(value = "advicetext") String adviceText,
	    @ModelAttribute(value = "username") String username,
	    @ModelAttribute(value = "adviceName") String adviceName) {
	LOGGER.info("Add advice page - Adding advice to DB");
	try {
	    adviceService.addAdvice(adviceName, adviceText, username);
	} catch (AdviceServiceException e) {
	    e.printStackTrace();
	    return "error";
	}
	return "advice saved";
    }

    /**
     * Return advice page
     * 
     * @param username
     * @param adviceId
     * @param map
     * @return
     */
    @RequestMapping(params = "action=getAdvicePage", method = RequestMethod.GET)
    public String getAdvicePage(
	    @ModelAttribute(value = "username") String username,
	    @ModelAttribute(value = "adviceId") String adviceId,
	    Map<String, Object> map) {
	try {
	    Advice advice = adviceService.findAdviceById(Long
		    .parseLong(adviceId));
	    map.put("advice", advice);
//	    map.put("adviceUsername", advice.getAdviceManager().getUser()
//		    .getUsername());
	    map.put("adviceText", getTextFromFile(advice.getAdviceFilePath()));
//	    map.put("adviceId", advice.getAdviceId());
//	    map.put("adviceName", advice.getAdvicename());
	} catch (NumberFormatException | AdviceServiceException
		| FileNotFoundException e) {
	    e.printStackTrace();
	}
	return "user_advicePage";
    }

    private String getTextFromFile(String path) throws FileNotFoundException {
	Scanner file = new Scanner(new File(path));
	if (file.hasNext()) {
	    return file.nextLine();
	}
	return "";
    }
    
    @RequestMapping(params = "action=addAdvice", method = RequestMethod.POST)
    @ResponseBody
    public String updateAdviceForUser(
	    @ModelAttribute(value = "adviceId") String adviceId,
	    @ModelAttribute(value = "adviceText") String adviceText,
	    @ModelAttribute(value = "username") String username,
	    @ModelAttribute(value = "adviceName") String adviceName) {
	LOGGER.info("Add advice page - Adding advice to DB");
	try {
	    adviceService.updateAdvice(adviceId, adviceName, adviceText, username);
	} catch (AdviceServiceException e) {
	    e.printStackTrace();
	    return "error";
	}
	return "advice saved";
    }
}
