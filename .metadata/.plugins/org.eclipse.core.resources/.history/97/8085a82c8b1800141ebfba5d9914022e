package com.my.pro.controller;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.my.pro.entity.Advice;
import com.my.pro.entity.AdviceManager;
import com.my.pro.entity.User;
import com.my.pro.exceptions.AdminPanelServiceException;
import com.my.pro.service.AdminPanelService;
import com.my.pro.service.UserService;
import com.my.pro.utils.PropertyLoader;

/**
 * Controller for managing advices
 * 
 * @author bkoles
 * 
 */
@Controller
@RequestMapping("secure_advice")
public class AdviceController {
    private static final Logger LOGGER = LoggerFactory
	    .getLogger(AdviceController.class);

    private static final String ADVICE_STORE_PATH = PropertyLoader
	    .loadProperty("advice.storePath");
    private static final String ADVICE_FOLDER_NAME = PropertyLoader
	    .loadProperty("advice.folderName");
    private static final String ADVICE_NAME = PropertyLoader
	    .loadProperty("advice.fileName");

    @Autowired
    private UserService userService;

    @Autowired
    private AdminPanelService adminPanelService;

    @RequestMapping(params = "action=getAddAdvicePage", method = RequestMethod.GET)
    public String getProfilePage() {
	LOGGER.info("Add advice page");
	return "user_addAdvicePage";
    }

    @RequestMapping(params = "action=addAdvice", method = RequestMethod.POST)
    @ResponseBody
    public String addAdviceForUser(@ModelAttribute(value = "json") String json,
	    @ModelAttribute(value = "username") String username,
	    @ModelAttribute(value = "advicename") String advicename) {
	User user = userService.findUserByName(username);
	AdviceManager adviceManager = user.getAdviceManager();
	if (adviceManager == null) {
	    adviceManager = new AdviceManager();
	    adviceManager.setUser(user);
	}

	String adviceStorePath = ADVICE_STORE_PATH + username
		+ ADVICE_FOLDER_NAME;
	new File(adviceStorePath).mkdir();

	String adviceName = username + ADVICE_NAME
		+ (adviceManager.getAdviceCounter() + 1) + ".json";

	FileWriter file;
	try {
	    file = new FileWriter(adviceStorePath + adviceName);
	    file.write(json);
	    file.flush();
	    file.close();
	} catch (IOException e) {
	    e.printStackTrace();
	    return "error";
	}
	Advice newAdvice = new Advice();
	newAdvice.setAdvicename(advicename);
	newAdvice.setAdviceFilePath(adviceStorePath + adviceName);
	newAdvice.setCtime(new Date());
	newAdvice.setAdviceManager(adviceManager);
	System.out.println(newAdvice);
	adviceManager.addAdviceToManager(newAdvice);
	user.setAdviceManager(adviceManager);

	try {
	    adminPanelService.updateUser(user);
	} catch (AdminPanelServiceException e) {
	    e.printStackTrace();
	    return "error";
	}

	return "advice saved";
    }

}
