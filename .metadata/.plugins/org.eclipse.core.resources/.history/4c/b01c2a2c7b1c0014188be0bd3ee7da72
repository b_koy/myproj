#cssmenu,
#cssmenu ul,
#cssmenu li,
#cssmenu span,
#cssmenu a {
  margin: 0;
  padding: 0;
  position: relative;
  border: 0;
  box-sizing: content-box;
}
#cssmenu {
  height: 30px;
  border-radius: 5px 5px 0 0;
  -moz-border-radius: 5px 5px 0 0;
  -webkit-border-radius: 5px 5px 0 0;
  background: #2B2B2B;
  background: -moz-linear-gradient(top, #2B2B2B 0%, #eee9f0 100%);
  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #2B2B2B), color-stop(100%, #eee9f0));
  background: -webkit-linear-gradient(top, #2B2B2B 0%, #eee9f0 100%);
  background: -o-linear-gradient(top, #2B2B2B 0%, #eee9f0 100%);
  background: -ms-linear-gradient(top, #2B2B2B 0%, #eee9f0 100%);
  background: linear-gradient(top, #2B2B2B 0%, #eee9f0 100%);
  border-bottom: 2px solid #69cc19;
  width: auto;
  color: white;
}
#cssmenu:after,
#cssmenu ul:after {
  content: '';
  display: block;
  clear: both;
}
#cssmenu a {
  background: #2B2B2B;
  background: -moz-linear-gradient(top, #2B2B2B 0%, #2B2B2B 100%);
  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #2B2B2B), color-stop(100%, #2B2B2B));
  background: -webkit-linear-gradient(top, #2B2B2B 0%, #2B2B2B 100%);
  background: -o-linear-gradient(top, #2B2B2B 0%, #2B2B2B 100%);
  background: -ms-linear-gradient(top, #2B2B2B 0%, #2B2B2B 100%);
  background: linear-gradient(top, #2B2B2B 0%, #2B2B2B 100%);
  color: #000;
  display: inline-block;
  font-family: Helvetica, Arial, Verdana, sans-serif;
  font-size: 12px;
  line-height: 30px;
  padding: 0 20px;
  text-decoration: none;
}
#cssmenu ul {
  list-style: none;
}
#cssmenu > ul {
  float: left;
}
#cssmenu > ul > li {
  float: left;
}
#cssmenu > ul > li > a {
  color: #000;
  font-size: 12px;
}
#cssmenu > ul > li:hover:after {
  content: '';
  display: block;
  width: 0;
  height: 0;
  position: absolute;
  left: 50%;
  bottom: 0;
  border-left: 10px solid transparent;
  border-right: 10px solid transparent;
  border-bottom: 10px solid #69cc19;
  margin-left: -10px;
}
#cssmenu > ul > li:first-child > a {
  border-radius: 5px 0 0 0;
  -moz-border-radius: 5px 0 0 0;
  -webkit-border-radius: 5px 0 0 0;
}
#cssmenu > ul > li.active:after {
  content: '';
  display: block;
  width: 0;
  height: 0;
  position: absolute;
  left: 50%;
  bottom: 0;
  border-left: 10px solid transparent;
  border-right: 10px solid transparent;
  border-bottom: 10px solid #69cc19;
  margin-left: -10px;
}
#cssmenu > ul > li.active > a {
  -moz-box-shadow: inset 0 0 2px rgba(0, 0, 0, 0.1);
  -webkit-box-shadow: inset 0 0 2px rgba(0, 0, 0, 0.1);
  box-shadow: inset 0 0 2px rgba(0, 0, 0, 0.1);
  background: #2B2B2B;
  background: -moz-linear-gradient(top, #2B2B2B 0%, #ffeeff ef 100%);
  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #2B2B2B), color-stop(100%, #ffeeff ef));
  background: -webkit-linear-gradient(top, #2B2B2B 0%, #ffeeff ef 100%);
  background: -o-linear-gradient(top, #2B2B2B 0%, #ffeeff ef 100%);
  background: -ms-linear-gradient(top, #2B2B2B 0%, #ffeeff ef 100%);
  background: linear-gradient(top, #2B2B2B 0%, #ffeeff ef 100%);
}
#cssmenu > ul > li:hover > a {
  background: #2B2B2B;
  background: -moz-linear-gradient(top, #2B2B2B 0%, #ffeeff ef 100%);
  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #2B2B2B), color-stop(100%, #ffeeff ef));
  background: -webkit-linear-gradient(top, #2B2B2B 0%, #ffeeff ef 100%);
  background: -o-linear-gradient(top, #2B2B2B 0%, #ffeeff ef 100%);
  background: -ms-linear-gradient(top, #2B2B2B 0%, #ffeeff ef 100%);
  background: linear-gradient(top, #2B2B2B 0%, #ffeeff ef 100%);
  -moz-box-shadow: inset 0 0 2px rgba(0, 0, 0, 0.1);
  -webkit-box-shadow: inset 0 0 2px rgba(0, 0, 0, 0.1);
  box-shadow: inset 0 0 2px rgba(0, 0, 0, 0.1);
}
#cssmenu .has-sub {
  z-index: 1;
}
#cssmenu .has-sub:hover > ul {
  display: block;
}
#cssmenu .has-sub ul {
  display: none;
  position: absolute;
  width: 200px;
  top: 100%;
  left: 0;
}
#cssmenu .has-sub ul li {
  *margin-bottom: -1px;
}
#cssmenu .has-sub ul li a {
  background: #69cc19;
  border-bottom: 1px dotted #82e632;
  filter: none;
  font-size: 11px;
  display: block;
  line-height: 100%;
  padding: 10px;
  color: #ffffff;
}
#cssmenu .has-sub ul li:hover a {
  background: #529f13;
}
#cssmenu .has-sub .has-sub:hover > ul {
  display: block;
}
#cssmenu .has-sub .has-sub ul {
  display: none;
  position: absolute;
  left: 100%;
  top: 0;
}
#cssmenu .has-sub .has-sub ul li a {
  background: #529f13;
  border-bottom: 1px dotted #82e632;
}
#cssmenu .has-sub .has-sub ul li a:hover {
  background: #468811;
}
