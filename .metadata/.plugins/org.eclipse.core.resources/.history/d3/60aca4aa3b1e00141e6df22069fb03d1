/**
 * 
 */
package com.my.pro.service.implementation;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.my.pro.dao.AdviceDao;
import com.my.pro.dao.CommentDao;
import com.my.pro.dao.UserDao;
import com.my.pro.entity.Advice;
import com.my.pro.entity.AdviceManager;
import com.my.pro.entity.Comment;
import com.my.pro.entity.User;
import com.my.pro.exceptions.AdviceServiceException;
import com.my.pro.service.AdviceService;
import com.my.pro.utils.PropertyLoader;

/**
 * @author bkoles
 * 
 */
@Service
public class AdviceServiceImpl implements AdviceService {

    private static final String ADVICE_STORE_PATH = PropertyLoader
	    .loadProperty("advice.storePath");
    private static final String ADVICE_FOLDER_NAME = PropertyLoader
	    .loadProperty("advice.folderName");
    private static final String ADVICE_NAME = PropertyLoader
	    .loadProperty("advice.fileName");
    private static final String ADVICE_FILE_EXTENSION = PropertyLoader
	    .loadProperty("advice.fileExtension");

    @Autowired
    UserDao userDao;

    @Autowired
    AdviceDao adviceDao;

    @Autowired
    CommentDao commentDao;

    /*
     * (non-Javadoc)
     * 
     * @see com.my.pro.service.AdviceService#addAdvice(java.lang.String,
     * java.lang.String, java.lang.String)
     */
    @Override
    @Transactional(readOnly = false, rollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
    public boolean addAdvice(String adviceName, String adviceText,
	    String username) throws AdviceServiceException {
	// Find user in DB
	User user = userDao.findByName(username);
	// Gets AdviceManager if exist, if not then creates it
	AdviceManager adviceManager = user.getAdviceManager();
	if (adviceManager == null) {
	    adviceManager = new AdviceManager();
	    adviceManager.setUser(user);
	}
	// Makes advice store path and create folders
	String adviceStorePath = ADVICE_STORE_PATH + username
		+ ADVICE_FOLDER_NAME;
	new File(ADVICE_STORE_PATH + username).mkdir();
	new File(adviceStorePath).mkdir();
	// Makes unique advice file name
	String adviceFileName = username + ADVICE_NAME
		+ (adviceManager.getAdviceCounter() + 1)
		+ ADVICE_FILE_EXTENSION;
	// Store advice file
	FileWriter file;
	try {
	    file = new FileWriter(adviceStorePath + adviceFileName);
	    file.write(adviceText);
	    file.flush();
	    file.close();
	} catch (IOException e) {
	    throw new AdviceServiceException("Can't create advice file.", e);
	}
	// Creates advice object and updates user
	Advice newAdvice = new Advice();
	newAdvice.setAdviceName(adviceName);
	newAdvice.setAdviceFilePath(adviceStorePath + adviceFileName);
	Date date = new Date();
	newAdvice.setCtime(date);
	newAdvice.setLetime(date);
	newAdvice.setAdviceManager(adviceManager);

	adviceManager.addAdviceToManager(newAdvice);
	System.out.println(adviceManager);
	user.setAdviceManager(adviceManager);
	userDao.update(user);

	return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.my.pro.service.AdviceService#findAdviceById(long)
     */
    @Override
    @Transactional
    public Advice findAdviceById(long id) throws AdviceServiceException {
	try {
	    return adviceDao.findAdviceById(id);
	} catch (Exception e) {
	    throw new AdviceServiceException("Advice Dao error.", e);
	}

    }

    @Override
    @Transactional
    public boolean updateAdvice(long adviceId, String adviceName,
	    String adviceText, String username) throws AdviceServiceException {

	Advice advice = adviceDao.findAdviceById(adviceId);
	// Makes advice store path and create folders
	String adviceStorePath = ADVICE_STORE_PATH + username
		+ ADVICE_FOLDER_NAME;

	// Makes unique advice file name
	String adviceFileName = username + ADVICE_NAME
		+ advice.getAdviceNumberInManager() + ADVICE_FILE_EXTENSION;
	// Store advice file
	FileWriter file;
	try {
	    file = new FileWriter(adviceStorePath + adviceFileName);
	    file.write(adviceText);
	    file.flush();
	    file.close();
	} catch (IOException e) {
	    throw new AdviceServiceException("Can't create advice file.", e);
	}
	// Update advice object and updates user

	advice.setAdviceName(adviceName);
	advice.setAdviceFilePath(adviceStorePath + adviceFileName);
	advice.setLetime(new Date());
	try {
	    adviceDao.updateAdvice(advice);
	} catch (Exception e) {
	    throw new AdviceServiceException("Can't update advice.", e);
	}
	return true;
    }

    @Override
    @Transactional
    public boolean removeAdvice(long adviceId, String username)
	    throws AdviceServiceException {
	Advice advice = adviceDao.findAdviceById(adviceId);
	String adviceFilePath = advice.getAdviceFilePath();
	try {
	    adviceDao.removeAdvice(advice);
	} catch (Exception e) {
	    throw new AdviceServiceException("Can't delete advice.", e);
	}
	new File(adviceFilePath).delete();
	return true;
    }

    @Override
    @Transactional
    public boolean addCommentToAdvice(long adviceId, String commentText,
	    String username) throws AdviceServiceException {
	User user = userDao.findByName(username);
	Advice advice = adviceDao.findAdviceById(adviceId);
	Set<Comment> commentSet = advice.getComments();
	if (commentSet == null) {
	    commentSet = new LinkedHashSet<>();
	}
	commentSet.add(new Comment(commentText, new Date(), advice, user
		.getAdviceManager()));
	advice.setComments(commentSet);
	try {
	    adviceDao.updateAdvice(advice);
	} catch (Exception e) {
	    throw new AdviceServiceException("Can't add comment to advice.", e);
	}
	return true;
    }

    @Override
    @Transactional
    public boolean removeCommentById(long commentId)
	    throws AdviceServiceException {
	commentDao.removeCommentById(commentId);
	return true;
    }

}
