package com.my.pro.entity;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "advice_manager")
public class AdviceManager {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "aManagerId", nullable = false)
    private long aManagerId;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "userId", nullable = false)
    private User user;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "adviceManager")
    private Set<Advice> advices;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "adviceManager")
    private Set<Comment> comments;

    @Column(name = "adviceCounter", nullable = false)
    private long adviceCounter;

    /**
     * @return the adviceCounter
     */
    public long getAdviceCounter() {
	return adviceCounter;
    }

    /**
     * @param adviceCounter
     *            the adviceCounter to set
     */
    public void setAdviceCounter(long adviceCounter) {
	this.adviceCounter = adviceCounter;
    }

    /**
     * 
     */
    public AdviceManager() {
	super();
    }

    /**
     * @return the aManagerId
     */
    public long getaManagerId() {
	return aManagerId;
    }

    /**
     * @param aManagerId
     *            the aManagerId to set
     */
    public void setaManagerId(long aManagerId) {
	this.aManagerId = aManagerId;
    }

    /**
     * @return the user
     */
    public User getUser() {
	return user;
    }

    /**
     * @param user
     *            the user to set
     */
    public void setUser(User user) {
	this.user = user;
    }

    /**
     * @return the advices
     */
    public Set<Advice> getAdvices() {
	return advices;
    }

    /**
     * @param advices
     *            the advices to set
     */
    public void setAdvices(Set<Advice> advices) {
	this.advices = advices;
    }

    /**
     * @return the comments
     */
    public Set<Comment> getComments() {
	return comments;
    }

    /**
     * @param comments
     *            the comments to set
     */
    public void setComments(Set<Comment> comments) {
	this.comments = comments;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return "AdviceManager [aManagerId=" + aManagerId + ", user=" + user
		+ ", advices=" + advices + ", comments=" + comments + "]";
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + (int) (aManagerId ^ (aManagerId >>> 32));
	result = prime * result + ((advices == null) ? 0 : advices.hashCode());
	result = prime * result
		+ ((comments == null) ? 0 : comments.hashCode());
	result = prime * result + ((user == null) ? 0 : user.hashCode());
	return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	AdviceManager other = (AdviceManager) obj;
	if (aManagerId != other.aManagerId)
	    return false;
	if (advices == null) {
	    if (other.advices != null)
		return false;
	} else if (!advices.equals(other.advices))
	    return false;
	if (comments == null) {
	    if (other.comments != null)
		return false;
	} else if (!comments.equals(other.comments))
	    return false;
	if (user == null) {
	    if (other.user != null)
		return false;
	} else if (!user.equals(other.user))
	    return false;
	return true;
    }

    /**
     * Add advice to AdviceManager
     * 
     * @param advice
     * @return
     */
    public boolean addAdviceToManager(Advice advice) {
	if (advices == null)
	    advices = new LinkedHashSet<>();
	advices.add(advice);
	adviceCounter++;
	return true;
    }

    /**
     * Add advice Set to AdviceManager
     * 
     * @param adviceSet
     * @return
     */
    public boolean addAdviceSetToManager(Set<Advice> adviceSet) {
	if (advices == null)
	    advices = new LinkedHashSet<>();
	advices.addAll(adviceSet);
	adviceCounter += adviceSet.size();
	return true;
    }

}
