package com.my.pro.entity;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "advices")
public class Advice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "adviceId", nullable = false)
    private long adviceId;

    @Column(name = "adviceName", nullable = true)
    private String advicename;

    @Column(name = "ctime", nullable = true)
    private Date ctime;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "aManagerId", nullable = false)
    private AdviceManager adviceManager;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "advice", cascade = CascadeType.ALL)
    private Set<Comment> comments;

    @Column(name = "adviceFilePath", nullable = true)
    private String adviceFilePath;

    /**
     * @return the adviceFilePath
     */
    public String getAdviceFilePath() {
	return adviceFilePath;
    }

    /**
     * @param adviceFilePath
     *            the adviceFilePath to set
     */
    public void setAdviceFilePath(String adviceFilePath) {
	this.adviceFilePath = adviceFilePath;
    }

    /**
     * 
     */
    public Advice() {
	super();
    }

    /**
     * @return the adviceId
     */
    public long getAdviceId() {
	return adviceId;
    }

    /**
     * @param adviceId
     *            the adviceId to set
     */
    public void setAdviceId(long adviceId) {
	this.adviceId = adviceId;
    }

    /**
     * @return the advicename
     */
    public String getAdvicename() {
	return advicename;
    }

    /**
     * @param advicename
     *            the advicename to set
     */
    public void setAdvicename(String advicename) {
	this.advicename = advicename;
    }

    /**
     * @return the ctime
     */
    public Date getCtime() {
	return ctime;
    }

    /**
     * @param ctime
     *            the ctime to set
     */
    public void setCtime(Date ctime) {
	this.ctime = ctime;
    }

    /**
     * @return the adviceManager
     */
    public AdviceManager getAdviceManager() {
	return adviceManager;
    }

    /**
     * @param adviceManager
     *            the adviceManager to set
     */
    public void setAdviceManager(AdviceManager adviceManager) {
	this.adviceManager = adviceManager;
    }

    /**
     * @return the comments
     */
    public Set<Comment> getComments() {
	return comments;
    }

    /**
     * @param comments
     *            the comments to set
     */
    public void setComments(Set<Comment> comments) {
	this.comments = comments;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return "Advice [adviceId=" + adviceId + ", advicename=" + advicename
		+ ", ctime=" + ctime + ", comments=" + comments + "]";
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    // @Override
    // public int hashCode() {
    // final int prime = 31;
    // int result = 1;
    // result = prime * result + (int) (adviceId ^ (adviceId >>> 32));
    // result = prime * result
    // + ((adviceManager == null) ? 0 : adviceManager.hashCode());
    // result = prime * result
    // + ((advicename == null) ? 0 : advicename.hashCode());
    // result = prime * result
    // + ((comments == null) ? 0 : comments.hashCode());
    // result = prime * result + ((ctime == null) ? 0 : ctime.hashCode());
    // return result;
    // }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Advice other = (Advice) obj;
	if (adviceId != other.adviceId)
	    return false;
	if (adviceManager == null) {
	    if (other.adviceManager != null)
		return false;
	} else if (!adviceManager.equals(other.adviceManager))
	    return false;
	if (advicename == null) {
	    if (other.advicename != null)
		return false;
	} else if (!advicename.equals(other.advicename))
	    return false;
	if (comments == null) {
	    if (other.comments != null)
		return false;
	} else if (!comments.equals(other.comments))
	    return false;
	if (ctime == null) {
	    if (other.ctime != null)
		return false;
	} else if (!ctime.equals(other.ctime))
	    return false;
	return true;
    }

}
