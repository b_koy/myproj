package com.my.pro.entity;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "advices")
@NamedQueries({ @NamedQuery(name = Advice.FIND_BY_ID, query = Advice.FIND_BY_ID_QUERY), })
public class Advice {
    public static final String FIND_BY_ID = "Advice.findByName";
    public static final String FIND_BY_ID_QUERY = "SELECT a FROM Advice a WHERE a.adviceId = ?1";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "adviceId", nullable = false)
    private long adviceId;

    @Column(name = "adviceName", nullable = false)
    private String adviceName;

    @Column(name = "ctime", nullable = true)
    private Date ctime;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "aManagerId", nullable = false)
    private AdviceManager adviceManager;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "advice", cascade = CascadeType.ALL)
    private Set<Comment> comments;

    @Column(name = "adviceFilePath", nullable = true)
    private String adviceFilePath;

    /**
     * @return the adviceFilePath
     */
    public String getAdviceFilePath() {
	return adviceFilePath;
    }

    /**
     * @param adviceFilePath
     *            the adviceFilePath to set
     */
    public void setAdviceFilePath(String adviceFilePath) {
	this.adviceFilePath = adviceFilePath;
    }

    /**
     * 
     */
    public Advice() {
	super();
    }

    /**
     * @return the adviceId
     */
    public long getAdviceId() {
	return adviceId;
    }

    /**
     * @param adviceId
     *            the adviceId to set
     */
    public void setAdviceId(long adviceId) {
	this.adviceId = adviceId;
    }

    /**
     * @return the advicename
     */
    public String getAdvicename() {
	return adviceName;
    }

    /**
     * @param advicename
     *            the advicename to set
     */
    public void setAdvicename(String advicename) {
	this.adviceName = advicename;
    }

    /**
     * @return the ctime
     */
    public Date getCtime() {
	return ctime;
    }

    /**
     * @param ctime
     *            the ctime to set
     */
    public void setCtime(Date ctime) {
	this.ctime = ctime;
    }

    /**
     * @return the adviceManager
     */
    public AdviceManager getAdviceManager() {
	return adviceManager;
    }

    /**
     * @param adviceManager
     *            the adviceManager to set
     */
    public void setAdviceManager(AdviceManager adviceManager) {
	this.adviceManager = adviceManager;
    }

    /**
     * @return the comments
     */
    public Set<Comment> getComments() {
	return comments;
    }

    /**
     * @param comments
     *            the comments to set
     */
    public void setComments(Set<Comment> comments) {
	this.comments = comments;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return "Advice [adviceId=" + adviceId + ", advicename=" + adviceName
		+ ", ctime=" + ctime + ", comments=" + comments + "]";
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + (int) (adviceId ^ (adviceId >>> 32));
	result = prime * result
		+ ((adviceName == null) ? 0 : adviceName.hashCode());
	result = prime * result + ((ctime == null) ? 0 : ctime.hashCode());
	return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Advice other = (Advice) obj;
	if (adviceId != other.adviceId)
	    return false;
	if (adviceManager == null) {
	    if (other.adviceManager != null)
		return false;
	} else if (!adviceManager.equals(other.adviceManager))
	    return false;
	if (adviceName == null) {
	    if (other.adviceName != null)
		return false;
	} else if (!adviceName.equals(other.adviceName))
	    return false;
	if (comments == null) {
	    if (other.comments != null)
		return false;
	} else if (!comments.equals(other.comments))
	    return false;
	if (ctime == null) {
	    if (other.ctime != null)
		return false;
	} else if (!ctime.equals(other.ctime))
	    return false;
	return true;
    }

}
