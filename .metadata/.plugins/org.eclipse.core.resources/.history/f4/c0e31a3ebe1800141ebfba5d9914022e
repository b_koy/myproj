/**
 * 
 */
package com.my.pro.service.implementation;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import com.my.pro.dao.UserDao;
import com.my.pro.entity.Advice;
import com.my.pro.entity.AdviceManager;
import com.my.pro.entity.User;
import com.my.pro.exceptions.AdviceServiceException;
import com.my.pro.service.AdviceService;
import com.my.pro.utils.PropertyLoader;

/**
 * @author bkoles
 * 
 */
public class AdviceServiceImpl implements AdviceService {

    private static final String ADVICE_STORE_PATH = PropertyLoader
	    .loadProperty("advice.storePath");
    private static final String ADVICE_FOLDER_NAME = PropertyLoader
	    .loadProperty("advice.folderName");
    private static final String ADVICE_NAME = PropertyLoader
	    .loadProperty("advice.fileName");
    private static final String ADVICE_FILE_EXTENSION = PropertyLoader
	    .loadProperty("advice.fileExtension");

    @Autowired
    UserDao userDao;

    /*
     * (non-Javadoc)
     * 
     * @see com.my.pro.service.AdviceService#addAdvice(java.lang.String,
     * java.lang.String, java.lang.String)
     */
    @Override
    public boolean addAdvice(String adviceName, String adviceText,
	    String username) throws AdviceServiceException {
	// Find user in DB
	User user = userDao.findByName(username);
	// Gets AdviceManager if exist, if not then creates it
	AdviceManager adviceManager = user.getAdviceManager();
	if (adviceManager == null) {
	    adviceManager = new AdviceManager();
	    adviceManager.setUser(user);
	}
	// Makes advice store path and create folders
	String adviceStorePath = ADVICE_STORE_PATH + username
		+ ADVICE_FOLDER_NAME;
	new File(adviceStorePath).mkdir();
	// Makes unique advice file name
	String adviceFileName = username + ADVICE_NAME
		+ (adviceManager.getAdviceCounter() + 1) + ADVICE_FILE_EXTENSION;
	// Store advice file
	FileWriter file;
	try {
	    file = new FileWriter(adviceStorePath + adviceFileName);
	    file.write(adviceText);
	    file.flush();
	    file.close();
	} catch (IOException e) {
	    throw new AdviceServiceException("Can't create advice file.", e);
	}
	// Creates advice object and updates user
	Advice newAdvice = new Advice();
	newAdvice.setAdvicename(adviceName);
	newAdvice.setAdviceFilePath(adviceStorePath + adviceFileName);
	newAdvice.setCtime(new Date());
	newAdvice.setAdviceManager(adviceManager);
	
	adviceManager.addAdviceToManager(newAdvice);
	
	user.setAdviceManager(adviceManager);
	userDao.update(user);

	return true;
    }
}
